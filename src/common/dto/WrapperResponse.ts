import { ApiProperty } from '@nestjs/swagger';
import { ResponseState } from '../enum/state';

export class WrapperResponse {
  @ApiProperty()
  readonly state: ResponseState ;

  @ApiProperty()
  readonly data: Object;

  constructor(state: ResponseState, data: Object) {
    this.state = state;
    this.data = data;
  }
}
