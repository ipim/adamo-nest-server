export enum ResponseState {
    OK = 'ok',
    ERROR = 'error',
    NO_CHANGES = 'no changes',
}
