import { INestApplication } from '@nestjs/common';
import { writeFileSync } from 'fs';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export function setupSwagger(app: INestApplication): void {
    const options = new DocumentBuilder()
        .setTitle('API')
        .setVersion('0.0.1')
        .addBearerAuth()
        .build();

  const document = SwaggerModule.createDocument(app, options);
  
  writeFileSync("./swagger-spec.json", JSON.stringify(document, null, 2));
  SwaggerModule.setup('docs', app, document);
}
