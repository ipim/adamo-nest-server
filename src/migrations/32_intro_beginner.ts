import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
import { CategoryEntity } from '../modules/tutorial/category/category.entity';
import { IntroEntity } from '../modules/tutorial/intro/intro.entity';
import {
    endEvent,
    lane,
    manualTask,
    messageFlow,
    pool,
    receiveTask,
    scriptTask,
    sendTask,
    sequenceFlow,
    startEvent,
    task,
    userTask,
} from './content/tutorial/beginner/index';
export class IntroBeginner1579848866970 implements MigrationInterface {
    // name = 'introBeginner1565611653967'

    public async up(queryRunner: QueryRunner): Promise<any> {
        console.log(' 32 - Intro Beginner - Retreiving category id');
        const getCategory_id_b = await queryRunner.manager
            .getRepository(CategoryEntity)
            .createQueryBuilder('category')
            .where('category.category_name = :category_name', {
                category_name: 'Beginner',
            })
            .getOne();
        const catBeginer = getCategory_id_b.category_id;
        let introIdentifier = 0;
        const md1 = await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(IntroEntity)
            .values([
                // 1.Start Event - EN
                {
                    intro_text: startEvent.content_en,
                    intro_text_de: startEvent.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },

                //--- 2.End Event --- EN ---
                {
                    intro_text: endEvent.content_en,
                    intro_text_de: endEvent.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },

                // --- 3.Task EN ---
                {
                    intro_text: task.content_en,
                    intro_text_de: task.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },

                // --- 4.User Task --- EN ---
                {
                    intro_text: userTask.content_en,
                    intro_text_de: userTask.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 5.Manual Task --- EN ------
                    intro_text: manualTask.content_en,
                    intro_text_de: manualTask.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },

                {
                    // ------ 6.Receive Task --- EN ------
                    intro_text: receiveTask.content_en,

                    // ------ 6.Receive Task DE ------
                    intro_text_de: receiveTask.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 7.Send Task EN ------
                    intro_text: sendTask.content_en,
                    intro_text_de: sendTask.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 8.Script Task EN ------
                    intro_text: scriptTask.content_en,
                    intro_text_de: scriptTask.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 9.Sequence Flow EN ------
                    intro_text: sequenceFlow.content_en,
                    intro_text_de: sequenceFlow.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 10.Message Flow EN ------
                    intro_text: messageFlow.content_en,
                    intro_text_de: messageFlow.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 11.Pool EN ------
                    intro_text: pool.content_en,
                    intro_text_de: pool.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
                {
                    // ------ 12.Lane EN ------
                    intro_text: lane.content_en,
                    intro_text_de: lane.content_de,
                    intro_categories: catBeginer,
                    intro_identifier: introIdentifier++,
                },
            ])
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        console.log(queryRunner);
    }
}
