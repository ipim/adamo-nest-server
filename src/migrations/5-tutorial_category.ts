import {MigrationInterface, QueryRunner} from "typeorm";
import { CategoryEntity } from "../modules/tutorial/category/category.entity";

export class tutorialCategory1578848866967 implements MigrationInterface {
    // name = 'tutorial_category1581879311947'

    public async up(queryRunner: QueryRunner): Promise<any> {
      
        console.log("5 - Creating Categories");
        await queryRunner.query(`CREATE TABLE "category" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "category_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "category_name" character varying NOT NULL, "category_identifier" integer NOT NULL, CONSTRAINT "PK_addfe3b3b96174012cd4710e975" PRIMARY KEY ("id", "category_id"))`);
       
        const seedCategory = await queryRunner.manager
        .createQueryBuilder()
        .insert()
        .into(CategoryEntity)
        .values([
           {
                category_name:"Beginner",
                category_identifier:1
            },
            {
                category_name:"Advanced",
                category_identifier:2
           },
            {
                category_name:"Professional",
                category_identifier:3
            },
        ])
        .execute();
 
 
    }

    public async down(queryRunner: QueryRunner): Promise<any> {}

}
