import {MigrationInterface, QueryRunner} from "typeorm";
import { CategoryEntity } from "../modules/tutorial/category/category.entity";

export class tutorial1578848866966 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        
        console.log("4 - Creating Tutorial Tables");

       await queryRunner.query(`CREATE TABLE "tg_modelling" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "tg_modelling_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "tg_modelling_question_id" character varying NOT NULL, "tg_modelling_xml_providet" character varying NOT NULL, "tg_modelling_validation_score" integer NOT NULL, "tg_modelling_editing_duration" integer NOT NULL, CONSTRAINT "PK_c38d193bec59fbc2d41dc2cee86" PRIMARY KEY ("id", "tg_modelling_id"))`);
      
        await queryRunner.query(`CREATE TABLE "test" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "test_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "test_solved_test_id" character varying NOT NULL, "test_user_id" character varying NOT NULL, "test_categorie" character varying NOT NULL, "test_tg_identifier" integer NOT NULL, "test_user_id_id" uuid, CONSTRAINT "PK_94272108dacdd24e4a5724f0206" PRIMARY KEY ("id", "test_id"))`);
        await queryRunner.query(`ALTER TABLE "test" ADD CONSTRAINT "FK_930bcc88be2e8b612ff7b34fd79" FOREIGN KEY ("test_user_id_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "test" DROP COLUMN "test_tg_identifier"`, undefined);

        // await queryRunner.query(`CREATE TABLE "intro" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "intro_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "intro_text" character varying NOT NULL, "intro_categories" character varying NOT NULL, "intro_identifier" character varying NOT NULL, "intro_id_id" character varying, "intro_id_tg_intro_id" character varying, CONSTRAINT "PK_3e7d0301e4ec445520fe00ad459" PRIMARY KEY ("id", "intro_id"))`);
             
        await queryRunner.query(`CREATE TABLE "tg_intro" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "tg_intro_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "tg_intro_intro_id" character varying NOT NULL, "tg_intro_is_finished" boolean NOT NULL, CONSTRAINT "PK_5d147643b8429fe69e4f24186cf" PRIMARY KEY ("id", "tg_intro_id"))`);
        await queryRunner.query(`ALTER TABLE "tg_intro" RENAME COLUMN "tg_intro_intro_id" TO "tg_intro_intro_category"`, undefined);
     
        await queryRunner.query(`CREATE TABLE "intro" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "intro_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "intro_text" character varying NOT NULL, "intro_text_de" character varying NOT NULL, "intro_categories" character varying NOT NULL, "intro_identifier" character varying NOT NULL, "intro_id_id" uuid, "intro_id_tg_intro_id" uuid, CONSTRAINT "PK_3e7d0301e4ec445520fe00ad459" PRIMARY KEY ("id", "intro_id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "intro" ADD CONSTRAINT "FK_7a07bd1a9c1a734742529c7643b" FOREIGN KEY ("intro_id_id", "intro_id_tg_intro_id") REFERENCES "tg_intro"("id","tg_intro_id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        
        await queryRunner.query(`CREATE TABLE "modelling_question" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "mod_qs_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "mod_qs_question_text" character varying NOT NULL, "mod_qs_categories" character varying NOT NULL, "mod_qs_custom_ruleset" character varying NOT NULL, CONSTRAINT "PK_c86c69d50d34709abfba0beeb5e" PRIMARY KEY ("id", "mod_qs_id"))`);
        await queryRunner.query(`ALTER TABLE "modelling_question" ADD "mod_qs_question_text_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" ADD "mod_qs_question_description_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" ADD "mod_qs_identifier" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" ADD "mod_qs_question_description" character varying NOT NULL`, undefined);
        
        await queryRunner.query(`CREATE TABLE "multiplechoice_question" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "multiplechoice_question_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "multiplechoice_question_text" character varying NOT NULL, "multiplechoice_question_categories" character varying NOT NULL, CONSTRAINT "PK_7cc114a23137064ad233d2cf7ca" PRIMARY KEY ("id", "multiplechoice_question_id"))`);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" ADD "multiplechoice_question_description" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" ADD "multiplechoice_question_text_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" ADD "multiplechoice_question_description_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" DROP COLUMN "multiplechoice_question_description"`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" DROP COLUMN "multiplechoice_question_description_de"`, undefined);
        
        await queryRunner.query(`CREATE TABLE "multiplechoice_question_answer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "multiplechoice_question_answer_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "multiplechoice_question_answer_question_id" character varying NOT NULL, "multiplechoice_question_answer_text" character varying NOT NULL, "multiplechoice_question_answer_true" boolean NOT NULL, CONSTRAINT "PK_70593a7cecd84fc5221e2461f96" PRIMARY KEY ("id", "multiplechoice_question_answer_id"))`);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question_answer" ADD "multiplechoice_question_answer_text_de" character varying NOT NULL`, undefined);
        
        await queryRunner.query(`CREATE TABLE "modelling_question_rules" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "modelling_question_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "modelling_rule_id" character varying NOT NULL, CONSTRAINT "PK_042a0e88e54af9e9faacae70f1c" PRIMARY KEY ("id", "modelling_question_id"))`);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" ADD "modelling_question_used" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" DROP COLUMN "modelling_rule_id"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" DROP COLUMN "modelling_question_used"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" ADD "modelling_question_rule_name" character varying NOT NULL`, undefined);
        
        await queryRunner.query(`CREATE TABLE "tg_multiplechoice_answered" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "tg_multiplechoice_answered_answer_id" character varying NOT NULL, "tg_multiplechoice_answered_answerd" boolean NOT NULL, CONSTRAINT "PK_3a8d2ff8ba692480e6e1fca1596" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" ADD "tg_multiplechoice_answered_id" uuid NOT NULL DEFAULT uuid_generate_v4()`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" DROP CONSTRAINT "PK_3a8d2ff8ba692480e6e1fca1596"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" ADD CONSTRAINT "PK_9190e8f4fe09b3a1cbdc8fdc3df" PRIMARY KEY ("id", "tg_multiplechoice_answered_id")`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" ADD "tg_multiplechoice_answered_from_qs_id" character varying NOT NULL`, undefined);
        
        await queryRunner.query(`CREATE TABLE "tg_multiplechoice" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "tg_multiplechoice_unique_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "tg_multiplechoice_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "tg_multiplechoice_multiplechoice_id" character varying NOT NULL, CONSTRAINT "PK_610d11dbdae1689923bdfd22fea" PRIMARY KEY ("id", "tg_multiplechoice_unique_id", "tg_multiplechoice_id"))`);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice" ADD "tg_multiplechoice_solved_correct" boolean NOT NULL`, undefined);
        
        await queryRunner.query(`CREATE TABLE "modelling_rules" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "modelling_rule_id" character varying NOT NULL, "modelling_rule_text" character varying NOT NULL, CONSTRAINT "PK_abbd94c3b5e9d55049cf604134f" PRIMARY KEY ("id"))`);
  
        await queryRunner.query(`ALTER TABLE "modelling_rules" ADD "modelling_rule_svg" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" ADD "modelling_rule_svg_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" ADD "modelling_rule_text_de" character varying NOT NULL`, undefined);
        
        // await queryRunner.query(`ALTER TABLE "models" ADD CONSTRAINT "UQ_28d23f5ebf773368621b7cfa91d" UNIQUE ("model_name")`, undefined);
        // await queryRunner.query(`ALTER TABLE "models" DROP CONSTRAINT "UQ_a6a29626d324b33c46efbb9c50b"`, undefined);
 
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "test" DROP CONSTRAINT "FK_930bcc88be2e8b612ff7b34fd79"`);
       // await queryRunner.query(`ALTER TABLE "intro" DROP CONSTRAINT "FK_7a07bd1a9c1a734742529c7643b"`);
        await queryRunner.query(`ALTER TABLE "models" ADD CONSTRAINT "UQ_a6a29626d324b33c46efbb9c50b" UNIQUE ("model_version")`);
        await queryRunner.query(`ALTER TABLE "models" DROP CONSTRAINT "UQ_28d23f5ebf773368621b7cfa91d"`);
        await queryRunner.query(`DROP TABLE "test"`);
        await queryRunner.query(`DROP TABLE "tg_modelling"`);
        await queryRunner.query(`DROP TABLE "tg_multiplechoice"`);
        await queryRunner.query(`DROP TABLE "tg_multiplechoice_answered"`);
        await queryRunner.query(`DROP TABLE "multiplechoice_question_answer"`);
        await queryRunner.query(`DROP TABLE "multiplechoice_question"`);
        await queryRunner.query(`DROP TABLE "modelling_question_rules"`);
        await queryRunner.query(`DROP TABLE "modelling_question"`);
        await queryRunner.query(`DROP TABLE "modelling_rules"`);
       // await queryRunner.query(`DROP TABLE "intro"`);
        await queryRunner.query(`DROP TABLE "tg_intro"`);
        await queryRunner.query(`DROP TABLE "category"`);
        // await queryRunner.query(`ALTER TABLE "models" RENAME COLUMN "model_name" TO "modelName"`);

        await queryRunner.query(`ALTER TABLE "tg_intro" RENAME COLUMN "tg_intro_intro_category" TO "tg_intro_intro_id"`, undefined);

        // 
        await queryRunner.query(`ALTER TABLE "tg_modelling" DROP COLUMN "tg_modelling_validation_score"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_modelling" ADD "tg_modelling_validation_score" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" DROP COLUMN "mod_qs_question_description"`, undefined);

        // 

        
        await queryRunner.query(`ALTER TABLE "tg_intro" DROP COLUMN "tg_intro_intro_id"`, undefined);

        
        await queryRunner.query(`ALTER TABLE "tg_intro" RENAME COLUMN "tg_intro_intro_category" TO "tg_intro_intro_id"`, undefined);
        
        // 

        
        await queryRunner.query(`ALTER TABLE "tg_intro" DROP COLUMN "tg_intro_is_finished"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_intro" ADD "tg_intro_is_finished" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_intro" RENAME COLUMN "tg_intro_is_finished" TO "tg_intro_last_clicked_id"`, undefined);

        await queryRunner.query(`ALTER TABLE "intro" DROP CONSTRAINT "FK_7a07bd1a9c1a734742529c7643b"`, undefined);
        await queryRunner.query(`DROP TABLE "intro"`, undefined);
        
        await queryRunner.query(`ALTER TABLE "tg_intro" DROP COLUMN "tg_intro_is_finished"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_intro" ADD "tg_intro_is_finished" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_intro" RENAME COLUMN "tg_intro_is_finished" TO "tg_intro_last_clicked_id"`, undefined);

        // 
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" DROP COLUMN "multiplechoice_question_description"`, undefined);
  

        await queryRunner.query(`ALTER TABLE "multiplechoice_question_answer" DROP COLUMN "multiplechoice_question_answer_text_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" DROP COLUMN "multiplechoice_question_description_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" DROP COLUMN "multiplechoice_question_text_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" DROP COLUMN "mod_qs_question_description_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question" DROP COLUMN "mod_qs_question_text_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "intro" DROP COLUMN "intro_text_de"`, undefined);
   
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" DROP COLUMN "modelling_question_used"`, undefined);
  
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" DROP COLUMN "tg_multiplechoice_answered_from_qs_id"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" DROP CONSTRAINT "PK_9190e8f4fe09b3a1cbdc8fdc3df"`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" ADD CONSTRAINT "PK_3a8d2ff8ba692480e6e1fca1596" PRIMARY KEY ("id")`, undefined);
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice_answered" DROP COLUMN "tg_multiplechoice_answered_id"`, undefined);
   
        await queryRunner.query(`ALTER TABLE "test" ADD "test_tg_identifier" integer NOT NULL`, undefined);
  
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" ADD "multiplechoice_question_description_de" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "multiplechoice_question" ADD "multiplechoice_question_description" character varying NOT NULL`, undefined);
 
        
        await queryRunner.query(`ALTER TABLE "tg_multiplechoice" DROP COLUMN "tg_multiplechoice_solved_correct"`, undefined);
  
        await queryRunner.query(`ALTER TABLE "modelling_rules" DROP COLUMN "modelling_rule_id"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" ADD "modelling_rule_id" uuid NOT NULL DEFAULT uuid_generate_v4()`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" DROP CONSTRAINT "PK_abbd94c3b5e9d55049cf604134f"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" ADD CONSTRAINT "PK_5f432bd76f437c435008a31f36c" PRIMARY KEY ("id", "modelling_rule_id")`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" DROP COLUMN "modelling_question_rule_name"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" ADD "modelling_question_used" character varying NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_question_rules" ADD "modelling_rule_id" character varying NOT NULL`, undefined);
   

        await queryRunner.query(`ALTER TABLE "modelling_rules" DROP COLUMN "modelling_rule_text_de"`, undefined);

        await queryRunner.query(`ALTER TABLE "modelling_rules" DROP COLUMN "modelling_rule_svg_de"`, undefined);
        await queryRunner.query(`ALTER TABLE "modelling_rules" DROP COLUMN "modelling_rule_svg"`, undefined);

        await queryRunner.query(`ALTER TABLE "tg_modelling" RENAME COLUMN "tg_modelling_editing_duration" TO "tg_modelling_editing_begin"`, undefined);

        await queryRunner.query(`ALTER TABLE "modelling_question" DROP COLUMN "mod_qs_identifier"`, undefined);
        await queryRunner.query(`ALTER TABLE "category" DROP COLUMN "category_identifier"`, undefined);

    }

}
