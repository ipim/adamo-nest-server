export const bpmnModel: string = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:bioc="http://bpmn.io/schema/bpmn/biocolor/1.0" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="1.11.3">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:extensionElements>
      <camunda:properties>
        <camunda:property name="IPIM_META_is_pre_picked" value="[is_pre_packed] == &#34;true&#34;" />
        <camunda:property name="IPIM_Val_is_pre_packed" value="&#34;true&#34;" />
        <camunda:property name="IPIM_Val_book_on_withdrawal" value="&#34;true&#34;" />
        <camunda:property name="IPIM_Val_picking_list_is_sorted" value="&#34;true&#34;" />
      </camunda:properties>
    </bpmn:extensionElements>
    <bpmn:startEvent id="StartEvent_1ao7h2z">
      <bpmn:outgoing>SequenceFlow_146d5js</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1iib1ql" name="Take picking list&#10;">
      <bpmn:incoming>SequenceFlow_146d5js</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0v4nk1q</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0wfjf4r" name="Choose picking tool for operation&#10;">
      <bpmn:incoming>SequenceFlow_16sgc7f</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0imj3qg</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0tzxav2</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_11w2nfw" name="Pick single parts / units&#10;">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0imj3qg</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0t86v39</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1ahsxza</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_06ym6cl</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_18z2t1b</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0xuu8nn" name="Place parts / units&#10; on picking tool&#10;">
      <bpmn:incoming>SequenceFlow_02qdatf</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_18z2t1b</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0vl2pnh</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0a56em6</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1otovfw</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0u6dlbe</bpmn:outgoing>
    </bpmn:task>
    <bpmn:exclusiveGateway id="ExclusiveGateway_0g1qulx" name="Further Picks">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0a56em6</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1xpddaw</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_08jdt7s</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1ahsxza</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1izioi7</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:task id="Task_1ann477" name="Confirm pick&#10;">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;false&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_08jdt7s</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1otovfw</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1mrq3ez</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0cunutq" name="Transport for handover&#10;">
      <bpmn:incoming>SequenceFlow_1mrq3ez</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0u6dlbe</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1izioi7</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_14vsbr3</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_0iisept">
      <bpmn:incoming>SequenceFlow_14vsbr3</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_1216lbx" name="Search next storage location&#10;">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[picking_list_is_sorted] == &#34;false&#34; &#38;&#38; [is_pre_picked] ==false" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1xpddaw</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0t86v39</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0adlbl1" name="Pick orderrelated prepacked parts / units&#10;">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked]" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0tzxav2</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1apdjy9</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_02qdatf</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_15x6ols" name="Confirm pick&#10;">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_06ym6cl</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1apdjy9</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0vl2pnh</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_146d5js" sourceRef="StartEvent_1ao7h2z" targetRef="Task_1iib1ql" />
    <bpmn:sequenceFlow id="SequenceFlow_0v4nk1q" sourceRef="Task_1iib1ql" targetRef="Task_0lkpo1o" />
    <bpmn:sequenceFlow id="SequenceFlow_0imj3qg" sourceRef="Task_0wfjf4r" targetRef="Task_11w2nfw">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0tzxav2" sourceRef="Task_0wfjf4r" targetRef="Task_0adlbl1">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked]" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0t86v39" sourceRef="Task_1216lbx" targetRef="Task_11w2nfw">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[picking_list_is_sorted] == &#34;false&#34; &#38;&#38; [is_pre_picked] ==false" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_06ym6cl" sourceRef="Task_11w2nfw" targetRef="Task_15x6ols">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0vl2pnh" sourceRef="Task_15x6ols" targetRef="Task_0xuu8nn">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0a56em6" sourceRef="Task_0xuu8nn" targetRef="ExclusiveGateway_0g1qulx">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1xpddaw" name="yes" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_1216lbx">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[picking_list_is_sorted] == &#34;false&#34; &#38;&#38; [is_pre_picked] ==false" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_08jdt7s" name="no" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_1ann477" />
    <bpmn:sequenceFlow id="SequenceFlow_1mrq3ez" sourceRef="Task_1ann477" targetRef="Task_0cunutq" />
    <bpmn:sequenceFlow id="SequenceFlow_14vsbr3" sourceRef="Task_0cunutq" targetRef="EndEvent_0iisept" />
    <bpmn:sequenceFlow id="SequenceFlow_1apdjy9" sourceRef="Task_0adlbl1" targetRef="Task_15x6ols">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked]" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1ahsxza" name="yes" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_11w2nfw">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[picking_list_is_sorted] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1otovfw" sourceRef="Task_0xuu8nn" targetRef="Task_1ann477">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] &#38;&#38; [book_on_withdrawal] == &#34;false&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0u6dlbe" sourceRef="Task_0xuu8nn" targetRef="Task_0cunutq">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] &#38;&#38; [book_on_withdrawal] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_02qdatf" sourceRef="Task_0adlbl1" targetRef="Task_0xuu8nn">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;false&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_18z2t1b" sourceRef="Task_11w2nfw" targetRef="Task_0xuu8nn">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;false&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1izioi7" name="no" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_0cunutq">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0lkpo1o" name="Start picking process&#10;">
      <bpmn:incoming>SequenceFlow_0v4nk1q</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_16sgc7f</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_16sgc7f" sourceRef="Task_0lkpo1o" targetRef="Task_0wfjf4r" />
    <bpmn:textAnnotation id="TextAnnotation_01ttvhz">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked] == false" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:text>!isPrePicked</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0dx7jpp">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[picking_list_is_sorted] == &#34;false&#34; &#38;&#38; [is_pre_picked] ==false" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:text>!picklistisSorted</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_00hnz28">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;true&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:text>bookOnWithdrawal</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0qwrs1a">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[book_on_withdrawal] == &#34;false&#34;" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:text>!bookOnWithdrawal</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0wa31x1">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[is_pre_picked]" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:text>isPrePicked</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:association id="Association_18u45vm" sourceRef="Task_11w2nfw" targetRef="TextAnnotation_01ttvhz" />
    <bpmn:association id="Association_0h7q9z9" sourceRef="Task_1ann477" targetRef="TextAnnotation_0qwrs1a" />
    <bpmn:association id="Association_0gcyox9" sourceRef="Task_1216lbx" targetRef="TextAnnotation_0dx7jpp" />
    <bpmn:association id="Association_1tqu6mv" sourceRef="Task_0adlbl1" targetRef="TextAnnotation_0wa31x1" />
    <bpmn:association id="Association_1c5a1zg" sourceRef="Task_15x6ols" targetRef="TextAnnotation_00hnz28" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="StartEvent_1ao7h2z_di" bpmnElement="StartEvent_1ao7h2z" bioc:stroke="black">
        <dc:Bounds x="278" y="433" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="-208" y="473" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1iib1ql_di" bpmnElement="Task_1iib1ql" bioc:stroke="black">
        <dc:Bounds x="341" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0wfjf4r_di" bpmnElement="Task_0wfjf4r" bioc:stroke="black">
        <dc:Bounds x="603" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_11w2nfw_di" bpmnElement="Task_11w2nfw" bioc:stroke="blue">
        <dc:Bounds x="734" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0xuu8nn_di" bpmnElement="Task_0xuu8nn" bioc:stroke="black">
        <dc:Bounds x="1109" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_0g1qulx_di" bpmnElement="ExclusiveGateway_0g1qulx" isMarkerVisible="true" bioc:stroke="blue">
        <dc:Bounds x="1235" y="426" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1228" y="480" width="66" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1ann477_di" bpmnElement="Task_1ann477" bioc:stroke="red">
        <dc:Bounds x="1325" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0cunutq_di" bpmnElement="Task_0cunutq" bioc:stroke="black">
        <dc:Bounds x="1470" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_0iisept_di" bpmnElement="EndEvent_0iisept" bioc:stroke="black">
        <dc:Bounds x="1612" y="433" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1540" y="473" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1216lbx_di" bpmnElement="Task_1216lbx" bioc:stroke="green">
        <dc:Bounds x="981" y="295" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_01ttvhz_di" bpmnElement="TextAnnotation_01ttvhz" bioc:stroke="blue">
        <dc:Bounds x="736" y="500" width="100" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0adlbl1_di" bpmnElement="Task_0adlbl1" bioc:stroke="fuchsia">
        <dc:Bounds x="734" y="557" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0dx7jpp_di" bpmnElement="TextAnnotation_0dx7jpp" bioc:stroke="green">
        <dc:Bounds x="970" y="242" width="122" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_15x6ols_di" bpmnElement="Task_15x6ols" bioc:stroke="darkviolet">
        <dc:Bounds x="929" y="437" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_00hnz28_di" bpmnElement="TextAnnotation_00hnz28" bioc:stroke="darkviolet">
        <dc:Bounds x="919" y="523" width="121" height="42" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0qwrs1a_di" bpmnElement="TextAnnotation_0qwrs1a" bioc:stroke="red">
        <dc:Bounds x="1317" y="340" width="126" height="42" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0wa31x1_di" bpmnElement="TextAnnotation_0wa31x1" bioc:stroke="fuchsia">
        <dc:Bounds x="734" y="653" width="99.99274099883856" height="29.997822299651567" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_146d5js_di" bpmnElement="SequenceFlow_146d5js" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="314" y="451" />
        <di:waypoint xsi:type="dc:Point" x="341" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="283" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0v4nk1q_di" bpmnElement="SequenceFlow_0v4nk1q" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="441" y="451" />
        <di:waypoint xsi:type="dc:Point" x="475" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="413" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0imj3qg_di" bpmnElement="SequenceFlow_0imj3qg" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="703" y="451" />
        <di:waypoint xsi:type="dc:Point" x="734" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="674" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0tzxav2_di" bpmnElement="SequenceFlow_0tzxav2" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="653" y="491" />
        <di:waypoint xsi:type="dc:Point" x="653" y="597" />
        <di:waypoint xsi:type="dc:Point" x="734" y="597" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="623" y="538" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0t86v39_di" bpmnElement="SequenceFlow_0t86v39" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="981" y="335" />
        <di:waypoint xsi:type="dc:Point" x="784" y="335" />
        <di:waypoint xsi:type="dc:Point" x="784" y="411" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="838" y="314" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_06ym6cl_di" bpmnElement="SequenceFlow_06ym6cl" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="834" y="451" />
        <di:waypoint xsi:type="dc:Point" x="882" y="451" />
        <di:waypoint xsi:type="dc:Point" x="929" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="813" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_18u45vm_di" bpmnElement="Association_18u45vm" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="785" y="491" />
        <di:waypoint xsi:type="dc:Point" x="786" y="500" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0vl2pnh_di" bpmnElement="SequenceFlow_0vl2pnh" bioc:stroke="darkviolet">
        <di:waypoint xsi:type="dc:Point" x="1031" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1069" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1109" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1005" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0a56em6_di" bpmnElement="SequenceFlow_0a56em6" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="1209" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1235" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1177" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1xpddaw_di" bpmnElement="SequenceFlow_1xpddaw" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="1259" y="427" />
        <di:waypoint xsi:type="dc:Point" x="1259" y="335" />
        <di:waypoint xsi:type="dc:Point" x="1081" y="335" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1220" y="375" width="19" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_08jdt7s_di" bpmnElement="SequenceFlow_08jdt7s" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1285" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1324" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1302" y="430" width="13" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1mrq3ez_di" bpmnElement="SequenceFlow_1mrq3ez" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1425" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1469" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1358" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_0h7q9z9_di" bpmnElement="Association_0h7q9z9" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1377" y="411" />
        <di:waypoint xsi:type="dc:Point" x="1378" y="382" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_14vsbr3_di" bpmnElement="SequenceFlow_14vsbr3" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1570" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1612" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1546" y="430" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_0gcyox9_di" bpmnElement="Association_0gcyox9" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1031" y="295" />
        <di:waypoint xsi:type="dc:Point" x="1031" y="272" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1apdjy9_di" bpmnElement="SequenceFlow_1apdjy9" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="834" y="597" />
        <di:waypoint xsi:type="dc:Point" x="877" y="597" />
        <di:waypoint xsi:type="dc:Point" x="877" y="477" />
        <di:waypoint xsi:type="dc:Point" x="929" y="477" />
        <di:waypoint xsi:type="dc:Point" x="929" y="477" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="847" y="531" width="90" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1tqu6mv_di" bpmnElement="Association_1tqu6mv" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="784" y="637" />
        <di:waypoint xsi:type="dc:Point" x="784" y="653" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1c5a1zg_di" bpmnElement="Association_1c5a1zg" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="979" y="517" />
        <di:waypoint xsi:type="dc:Point" x="979" y="523" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1ahsxza_di" bpmnElement="SequenceFlow_1ahsxza" bioc:stroke="royalblue">
        <di:waypoint xsi:type="dc:Point" x="1260" y="426" />
        <di:waypoint xsi:type="dc:Point" x="1260" y="224" />
        <di:waypoint xsi:type="dc:Point" x="784" y="224" />
        <di:waypoint xsi:type="dc:Point" x="784" y="411" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1221" y="374" width="19" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1otovfw_di" bpmnElement="SequenceFlow_1otovfw" bioc:stroke="crimson">
        <di:waypoint xsi:type="dc:Point" x="1159" y="491" />
        <di:waypoint xsi:type="dc:Point" x="1159" y="562" />
        <di:waypoint xsi:type="dc:Point" x="1375" y="562" />
        <di:waypoint xsi:type="dc:Point" x="1375" y="491" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1222" y="540.5" width="90" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0u6dlbe_di" bpmnElement="SequenceFlow_0u6dlbe" bioc:stroke="aquamarine">
        <di:waypoint xsi:type="dc:Point" x="1159" y="491" />
        <di:waypoint xsi:type="dc:Point" x="1159" y="641" />
        <di:waypoint xsi:type="dc:Point" x="1520" y="641" />
        <di:waypoint xsi:type="dc:Point" x="1520" y="491" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1295" y="620" width="90" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_02qdatf_di" bpmnElement="SequenceFlow_02qdatf" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="834" y="597" />
        <di:waypoint xsi:type="dc:Point" x="1121" y="597" />
        <di:waypoint xsi:type="dc:Point" x="1121" y="491" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="933" y="576" width="90" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_18z2t1b_di" bpmnElement="SequenceFlow_18z2t1b" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="834" y="429" />
        <di:waypoint xsi:type="dc:Point" x="1075" y="429" />
        <di:waypoint xsi:type="dc:Point" x="1109" y="429" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="910" y="408" width="90" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1izioi7_di" bpmnElement="SequenceFlow_1izioi7" bioc:stroke="darkviolet">
        <di:waypoint xsi:type="dc:Point" x="1285" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1295" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1295" y="393" />
        <di:waypoint xsi:type="dc:Point" x="1520" y="393" />
        <di:waypoint xsi:type="dc:Point" x="1520" y="411" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1302" y="430" width="13" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_0lkpo1o_di" bpmnElement="Task_0lkpo1o" bioc:stroke="black">
        <dc:Bounds x="475" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_16sgc7f_di" bpmnElement="SequenceFlow_16sgc7f" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="575" y="451" />
        <di:waypoint xsi:type="dc:Point" x="603" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="544" y="430" width="90" height="13" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>

`
