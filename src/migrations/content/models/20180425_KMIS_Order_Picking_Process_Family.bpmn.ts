export const bpmnModel: string = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:bioc="http://bpmn.io/schema/bpmn/biocolor/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="1.11.3">
  <bpmn:process id="Process_1" isExecutable="true">
    <bpmn:startEvent id="StartEvent_1ao7h2z">
      <bpmn:outgoing>SequenceFlow_146d5js</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1iib1ql" name="Kommissionierliste / Betriebsauftrag entnehmen">
      <bpmn:incoming>SequenceFlow_146d5js</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0v4nk1q</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0wfjf4r" name="Kommissioniervorgang starten">
      <bpmn:incoming>SequenceFlow_0v4nk1q</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0imj3qg</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0tzxav2</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_11w2nfw" name="Einzelartikel entnehmen">
      <bpmn:incoming>SequenceFlow_0imj3qg</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0t86v39</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_06ym6cl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_1knxmn8" name="Kommissionierhilfe auswählen">
      <bpmn:incoming>SequenceFlow_0vl2pnh</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0nnsazd</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0xuu8nn" name="Abgabe der Entnahme">
      <bpmn:incoming>SequenceFlow_0nnsazd</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0a56em6</bpmn:outgoing>
    </bpmn:task>
    <bpmn:exclusiveGateway id="ExclusiveGateway_0g1qulx" name="Further Picks">
      <bpmn:incoming>SequenceFlow_0a56em6</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1xpddaw</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_08jdt7s</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:task id="Task_1ann477" name="Entnahme Quittieren">
      <bpmn:incoming>SequenceFlow_08jdt7s</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1mrq3ez</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0cunutq" name="Transport der Sammeleinheit zur Abgabe">
      <bpmn:incoming>SequenceFlow_1mrq3ez</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_14vsbr3</bpmn:outgoing>
    </bpmn:task>
    <bpmn:endEvent id="EndEvent_0iisept">
      <bpmn:incoming>SequenceFlow_14vsbr3</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:task id="Task_1216lbx" name="Lagerplatz suchen">
      <bpmn:incoming>SequenceFlow_1xpddaw</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0t86v39</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0adlbl1" name="Auftragsbezogene vorkomissionierte Artikel entnehmen">
      <bpmn:incoming>SequenceFlow_0tzxav2</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1apdjy9</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_15x6ols" name="Entnahme Quittieren">
      <bpmn:incoming>SequenceFlow_06ym6cl</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1apdjy9</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0vl2pnh</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_146d5js" sourceRef="StartEvent_1ao7h2z" targetRef="Task_1iib1ql" />
    <bpmn:sequenceFlow id="SequenceFlow_0v4nk1q" sourceRef="Task_1iib1ql" targetRef="Task_0wfjf4r" />
    <bpmn:sequenceFlow id="SequenceFlow_0imj3qg" sourceRef="Task_0wfjf4r" targetRef="Task_11w2nfw" />
    <bpmn:sequenceFlow id="SequenceFlow_0tzxav2" sourceRef="Task_0wfjf4r" targetRef="Task_0adlbl1" />
    <bpmn:sequenceFlow id="SequenceFlow_0t86v39" sourceRef="Task_1216lbx" targetRef="Task_11w2nfw" />
    <bpmn:sequenceFlow id="SequenceFlow_06ym6cl" sourceRef="Task_11w2nfw" targetRef="Task_15x6ols" />
    <bpmn:sequenceFlow id="SequenceFlow_0vl2pnh" sourceRef="Task_15x6ols" targetRef="Task_1knxmn8" />
    <bpmn:sequenceFlow id="SequenceFlow_0nnsazd" sourceRef="Task_1knxmn8" targetRef="Task_0xuu8nn" />
    <bpmn:sequenceFlow id="SequenceFlow_0a56em6" sourceRef="Task_0xuu8nn" targetRef="ExclusiveGateway_0g1qulx" />
    <bpmn:sequenceFlow id="SequenceFlow_1xpddaw" name="yes" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_1216lbx" />
    <bpmn:sequenceFlow id="SequenceFlow_08jdt7s" name="no" sourceRef="ExclusiveGateway_0g1qulx" targetRef="Task_1ann477" />
    <bpmn:sequenceFlow id="SequenceFlow_1mrq3ez" sourceRef="Task_1ann477" targetRef="Task_0cunutq" />
    <bpmn:sequenceFlow id="SequenceFlow_14vsbr3" sourceRef="Task_0cunutq" targetRef="EndEvent_0iisept" />
    <bpmn:sequenceFlow id="SequenceFlow_1apdjy9" sourceRef="Task_0adlbl1" targetRef="Task_15x6ols" />
    <bpmn:textAnnotation id="TextAnnotation_01ttvhz">
      <bpmn:text>!isPrePicked</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0dx7jpp">
      <bpmn:text>!picklistisSorted</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_00hnz28">
      <bpmn:text>bookOnWithdrawal</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0qwrs1a">
      <bpmn:text>!bookOnWithdrawal</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:textAnnotation id="TextAnnotation_0wa31x1">
      <bpmn:text>isPrePicked</bpmn:text>
    </bpmn:textAnnotation>
    <bpmn:association id="Association_18u45vm" sourceRef="Task_11w2nfw" targetRef="TextAnnotation_01ttvhz" />
    <bpmn:association id="Association_0h7q9z9" sourceRef="Task_1ann477" targetRef="TextAnnotation_0qwrs1a" />
    <bpmn:association id="Association_0gcyox9" sourceRef="Task_1216lbx" targetRef="TextAnnotation_0dx7jpp" />
    <bpmn:association id="Association_1tqu6mv" sourceRef="Task_0adlbl1" targetRef="TextAnnotation_0wa31x1" />
    <bpmn:association id="Association_1c5a1zg" sourceRef="Task_15x6ols" targetRef="TextAnnotation_00hnz28" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="StartEvent_1ao7h2z_di" bpmnElement="StartEvent_1ao7h2z">
        <dc:Bounds x="382" y="433" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="-59" y="473" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1iib1ql_di" bpmnElement="Task_1iib1ql">
        <dc:Bounds x="436" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0wfjf4r_di" bpmnElement="Task_0wfjf4r">
        <dc:Bounds x="558" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_11w2nfw_di" bpmnElement="Task_11w2nfw" bioc:stroke="#1E88E5" bioc:fill="#BBDEFB">
        <dc:Bounds x="734" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1knxmn8_di" bpmnElement="Task_1knxmn8">
        <dc:Bounds x="1054" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0xuu8nn_di" bpmnElement="Task_0xuu8nn">
        <dc:Bounds x="1179" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_0g1qulx_di" bpmnElement="ExclusiveGateway_0g1qulx" isMarkerVisible="true">
        <dc:Bounds x="1308" y="426" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1301" y="480" width="66" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1ann477_di" bpmnElement="Task_1ann477" bioc:stroke="#FB8C00" bioc:fill="#FFE0B2">
        <dc:Bounds x="1398" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0cunutq_di" bpmnElement="Task_0cunutq">
        <dc:Bounds x="1543" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_0iisept_di" bpmnElement="EndEvent_0iisept">
        <dc:Bounds x="1695" y="433" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1668" y="473" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1216lbx_di" bpmnElement="Task_1216lbx" bioc:stroke="#43A047" bioc:fill="#C8E6C9">
        <dc:Bounds x="1083" y="278" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_01ttvhz_di" bpmnElement="TextAnnotation_01ttvhz">
        <dc:Bounds x="736" y="500" width="100" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0adlbl1_di" bpmnElement="Task_0adlbl1" bioc:stroke="#E53935" bioc:fill="#FFCDD2">
        <dc:Bounds x="734" y="557" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0dx7jpp_di" bpmnElement="TextAnnotation_0dx7jpp">
        <dc:Bounds x="1083" y="371" width="99.99202297383536" height="29.993618379068284" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_15x6ols_di" bpmnElement="Task_15x6ols" bioc:stroke="#8E24AA" bioc:fill="#E1BEE7">
        <dc:Bounds x="919" y="411" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_00hnz28_di" bpmnElement="TextAnnotation_00hnz28">
        <dc:Bounds x="908" y="506" width="121" height="42" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0qwrs1a_di" bpmnElement="TextAnnotation_0qwrs1a">
        <dc:Bounds x="1393" y="506" width="126" height="42" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="TextAnnotation_0wa31x1_di" bpmnElement="TextAnnotation_0wa31x1">
        <dc:Bounds x="734" y="653" width="99.99274099883856" height="29.997822299651567" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_146d5js_di" bpmnElement="SequenceFlow_146d5js">
        <di:waypoint xsi:type="dc:Point" x="418" y="451" />
        <di:waypoint xsi:type="dc:Point" x="436" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="427" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0v4nk1q_di" bpmnElement="SequenceFlow_0v4nk1q">
        <di:waypoint xsi:type="dc:Point" x="536" y="451" />
        <di:waypoint xsi:type="dc:Point" x="558" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="547" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0imj3qg_di" bpmnElement="SequenceFlow_0imj3qg">
        <di:waypoint xsi:type="dc:Point" x="658" y="451" />
        <di:waypoint xsi:type="dc:Point" x="734" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="696" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0tzxav2_di" bpmnElement="SequenceFlow_0tzxav2">
        <di:waypoint xsi:type="dc:Point" x="658" y="451" />
        <di:waypoint xsi:type="dc:Point" x="696" y="451" />
        <di:waypoint xsi:type="dc:Point" x="696" y="597" />
        <di:waypoint xsi:type="dc:Point" x="734" y="597" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="711" y="518" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0t86v39_di" bpmnElement="SequenceFlow_0t86v39">
        <di:waypoint xsi:type="dc:Point" x="1083" y="318" />
        <di:waypoint xsi:type="dc:Point" x="784" y="318" />
        <di:waypoint xsi:type="dc:Point" x="784" y="411" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="933.5" y="297" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_06ym6cl_di" bpmnElement="SequenceFlow_06ym6cl">
        <di:waypoint xsi:type="dc:Point" x="834" y="451" />
        <di:waypoint xsi:type="dc:Point" x="882" y="451" />
        <di:waypoint xsi:type="dc:Point" x="882" y="451" />
        <di:waypoint xsi:type="dc:Point" x="919" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="897" y="445" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_18u45vm_di" bpmnElement="Association_18u45vm">
        <di:waypoint xsi:type="dc:Point" x="785" y="491" />
        <di:waypoint xsi:type="dc:Point" x="786" y="500" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0vl2pnh_di" bpmnElement="SequenceFlow_0vl2pnh">
        <di:waypoint xsi:type="dc:Point" x="1019" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1054" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1036.5" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0nnsazd_di" bpmnElement="SequenceFlow_0nnsazd">
        <di:waypoint xsi:type="dc:Point" x="1154" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1179" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1166.5" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0a56em6_di" bpmnElement="SequenceFlow_0a56em6">
        <di:waypoint xsi:type="dc:Point" x="1279" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1308" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1293.5" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1xpddaw_di" bpmnElement="SequenceFlow_1xpddaw">
        <di:waypoint xsi:type="dc:Point" x="1332" y="426" />
        <di:waypoint xsi:type="dc:Point" x="1332" y="318" />
        <di:waypoint xsi:type="dc:Point" x="1183" y="318" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1338" y="366" width="19" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_08jdt7s_di" bpmnElement="SequenceFlow_08jdt7s">
        <di:waypoint xsi:type="dc:Point" x="1358" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1397" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1373.5" y="430" width="12" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1mrq3ez_di" bpmnElement="SequenceFlow_1mrq3ez">
        <di:waypoint xsi:type="dc:Point" x="1498" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1542" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1476" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_0h7q9z9_di" bpmnElement="Association_0h7q9z9">
        <di:waypoint xsi:type="dc:Point" x="1452" y="491" />
        <di:waypoint xsi:type="dc:Point" x="1453" y="506" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_14vsbr3_di" bpmnElement="SequenceFlow_14vsbr3">
        <di:waypoint xsi:type="dc:Point" x="1643" y="451" />
        <di:waypoint xsi:type="dc:Point" x="1694" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1625.5" y="430" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_0gcyox9_di" bpmnElement="Association_0gcyox9">
        <di:waypoint xsi:type="dc:Point" x="1133" y="358" />
        <di:waypoint xsi:type="dc:Point" x="1133" y="371" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1apdjy9_di" bpmnElement="SequenceFlow_1apdjy9">
        <di:waypoint xsi:type="dc:Point" x="834" y="597" />
        <di:waypoint xsi:type="dc:Point" x="877" y="597" />
        <di:waypoint xsi:type="dc:Point" x="877" y="451" />
        <di:waypoint xsi:type="dc:Point" x="919" y="451" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="892" y="518" width="0" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1tqu6mv_di" bpmnElement="Association_1tqu6mv">
        <di:waypoint xsi:type="dc:Point" x="784" y="637" />
        <di:waypoint xsi:type="dc:Point" x="784" y="653" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="Association_1c5a1zg_di" bpmnElement="Association_1c5a1zg">
        <di:waypoint xsi:type="dc:Point" x="969" y="491" />
        <di:waypoint xsi:type="dc:Point" x="969" y="506" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
`
