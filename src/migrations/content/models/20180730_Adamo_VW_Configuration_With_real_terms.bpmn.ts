export const bpmnModel: string = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:bioc="http://bpmn.io/schema/bpmn/biocolor/1.0" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="1.3.2">
  <bpmn:process id="Process_1" isExecutable="false">
    <bpmn:extensionElements>
      <camunda:properties>
        <camunda:property name="IPIM_Val_alterletztepruefung" value="3" />
        <camunda:property name="IPIM_Val_buergschaftnoetig" value="true" />
        <camunda:property name="IPIM_Val_kaufinteressevorhanden" value="true" />
        <camunda:property name="IPIM_Val_verlaenerungMoeglich" value="true" />
      </camunda:properties>
    </bpmn:extensionElements>
    <bpmn:startEvent id="StartEvent_1">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:outgoing>SequenceFlow_0bmc6tz</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:task id="Task_1omw4ku" name="Auswahl des Fahrzeuges">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0bmc6tz</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0vbongb</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0bmc6tz" sourceRef="StartEvent_1" targetRef="Task_1omw4ku">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0ohg6e3" name="Konfiguration des Fahrzeuges">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0vbongb</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0xa1iwl</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0vbongb" sourceRef="Task_1omw4ku" targetRef="Task_0ohg6e3">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0q0cw3u" name="Berechnung der Leasingrate">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0u0a7y4</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_05l0ut4</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0a4t20v</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0xzg1jf" name="Bestimmung der Laufzeit">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0xa1iwl</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_13o5lwi</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0hmi9dn</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0xa1iwl" sourceRef="Task_0ohg6e3" targetRef="Task_0xzg1jf">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0czf8dr" name="Bonität prüfen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_13o5lwi</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_00ib3gt</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_13o5lwi" sourceRef="Task_0xzg1jf" targetRef="Task_0czf8dr">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:exclusiveGateway id="ExclusiveGateway_11vfkka" name="Bonität Ok?">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_00ib3gt</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0u0a7y4</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0lzglr0</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="SequenceFlow_00ib3gt" sourceRef="Task_0czf8dr" targetRef="ExclusiveGateway_11vfkka">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0u0a7y4" name="Ja" sourceRef="ExclusiveGateway_11vfkka" targetRef="Task_0q0cw3u">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression" />
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0lzglr0" name="nein" sourceRef="ExclusiveGateway_11vfkka" targetRef="EndEvent_0iyib5c">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:endEvent id="EndEvent_0iyib5c" name="Prozess beenden">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[BuergschaftNoetig]=false AND [AlterLetztePruefung] &#62;3" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0lzglr0</bpmn:incoming>
      <bpmn:terminateEventDefinition />
    </bpmn:endEvent>
    <bpmn:task id="Task_0chnlrf" name="Fahrzeug übergben">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0a4t20v</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0hpvyjm</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_19mdcmi</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0a4t20v" sourceRef="Task_0q0cw3u" targetRef="Task_0chnlrf">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_19mdcmi" sourceRef="Task_0chnlrf" targetRef="IntermediateThrowEvent_0gcgzel">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_1bovh02" name="Fahrzeug zurück nehmen">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_040cv74</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1h3ntrj</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_1r7gzma</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1sz6ye3</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_040cv74" sourceRef="IntermediateThrowEvent_0gcgzel" targetRef="Task_1bovh02">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:endEvent id="EndEvent_0c9infk" name="Leasing beendet">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1sz6ye3</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1sz6ye3" sourceRef="Task_1bovh02" targetRef="EndEvent_0c9infk">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:intermediateCatchEvent id="IntermediateThrowEvent_0gcgzel" name="Laufzeit abgelaufen">
      <bpmn:extensionElements>
        <camunda:properties />
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_19mdcmi</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_040cv74</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_09e7uxu</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_0hzy7k8</bpmn:outgoing>
      <bpmn:timerEventDefinition />
    </bpmn:intermediateCatchEvent>
    <bpmn:task id="Task_0fpi4xt" name="Bürgschaft einhohlen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0hxlup9</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0hpvyjm</bpmn:outgoing>
    </bpmn:task>
    <bpmn:task id="Task_0jkktt6" name="Verlägerung abfragen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_09e7uxu</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1squaca</bpmn:outgoing>
    </bpmn:task>
    <bpmn:exclusiveGateway id="ExclusiveGateway_07zq1sx" name="Verlängern?">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1squaca</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1h3ntrj</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_05l0ut4</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="SequenceFlow_1squaca" sourceRef="Task_0jkktt6" targetRef="ExclusiveGateway_07zq1sx">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_106y2vc" name="Kaufinteresse abfragen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0hzy7k8</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0nppu2d</bpmn:outgoing>
    </bpmn:task>
    <bpmn:exclusiveGateway id="ExclusiveGateway_1lodnfn" name="Kunde kauft?">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0nppu2d</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1r7gzma</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_13iuw1h</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:sequenceFlow id="SequenceFlow_0nppu2d" sourceRef="Task_106y2vc" targetRef="ExclusiveGateway_1lodnfn">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1r7gzma" name="nein" sourceRef="ExclusiveGateway_1lodnfn" targetRef="Task_1bovh02">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_0rpafiw" name="Kaufvertrag erstellen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_13iuw1h</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0go5rf4</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_13iuw1h" name="ja" sourceRef="ExclusiveGateway_1lodnfn" targetRef="Task_0rpafiw">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_15c0efz" name="Fahrzeug übereignen">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0go5rf4</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1iu0x65</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0go5rf4" sourceRef="Task_0rpafiw" targetRef="Task_15c0efz">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:endEvent id="EndEvent_0zjjaeu" name="Fahrzwug verkauf">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_1iu0x65</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:sequenceFlow id="SequenceFlow_1iu0x65" sourceRef="Task_15c0efz" targetRef="EndEvent_0zjjaeu">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:task id="Task_1n1e5yk" name="Berechnung der Leasingrate">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
      <bpmn:incoming>SequenceFlow_0hmi9dn</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0hxlup9</bpmn:outgoing>
    </bpmn:task>
    <bpmn:sequenceFlow id="SequenceFlow_0hxlup9" sourceRef="Task_1n1e5yk" targetRef="Task_0fpi4xt">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0hpvyjm" sourceRef="Task_0fpi4xt" targetRef="Task_0chnlrf">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0hmi9dn" sourceRef="Task_0xzg1jf" targetRef="Task_1n1e5yk">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_09e7uxu" sourceRef="IntermediateThrowEvent_0gcgzel" targetRef="Task_0jkktt6">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1h3ntrj" name="nein" sourceRef="ExclusiveGateway_07zq1sx" targetRef="Task_1bovh02">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_05l0ut4" name="ja" sourceRef="ExclusiveGateway_07zq1sx" targetRef="Task_0q0cw3u">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[verlaenerungMoeglich]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_0hzy7k8" sourceRef="IntermediateThrowEvent_0gcgzel" targetRef="Task_106y2vc">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[kaufInteresseVorhanden]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:sequenceFlow>
    <bpmn:textAnnotation id="TextAnnotation_1xz27vz">    <bpmn:text>[BuergschaftNoetig]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_12jwgy7" sourceRef="Task_1n1e5yk" targetRef="TextAnnotation_1xz27vz">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:association>
    <bpmn:textAnnotation id="TextAnnotation_0t71a3a">    <bpmn:text>[BuergschaftNoetig]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_1h5m3ew" sourceRef="Task_0fpi4xt" targetRef="TextAnnotation_0t71a3a">
      <bpmn:extensionElements>
        <camunda:properties>
          <camunda:property name="IPIM_Calc" value="[buergschaftNoetig]=true" />
        </camunda:properties>
      </bpmn:extensionElements>
    </bpmn:association>
    <bpmn:textAnnotation id="TextAnnotation_1d9slmw">    <bpmn:text><![CDATA[[BuergschaftNoetig]=false AND [AlterLetztePruefung] >3]]></bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_09768cp" sourceRef="Task_0czf8dr" targetRef="TextAnnotation_1d9slmw" />
    <bpmn:textAnnotation id="TextAnnotation_01sih2u">    <bpmn:text>[kaufInteresseVorhanden]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_1u2lasa" sourceRef="Task_106y2vc" targetRef="TextAnnotation_01sih2u" />
    <bpmn:textAnnotation id="TextAnnotation_1b8lq1s">    <bpmn:text>[verlaenerungMoeglich]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_03ctd2m" sourceRef="Task_0jkktt6" targetRef="TextAnnotation_1b8lq1s" />
    <bpmn:textAnnotation id="TextAnnotation_1x0b7uw">    <bpmn:text>[kaufInteresseVorhanden]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_01evohe" sourceRef="Task_0rpafiw" targetRef="TextAnnotation_1x0b7uw" />
    <bpmn:textAnnotation id="TextAnnotation_05j2p0k">    <bpmn:text>[kaufInteresseVorhanden]=true</bpmn:text>
</bpmn:textAnnotation>
    <bpmn:association id="Association_0a9n6um" sourceRef="Task_15c0efz" targetRef="TextAnnotation_05j2p0k" />
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1" bioc:stroke="black">
        <dc:Bounds x="289" y="73" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="262" y="109" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_1omw4ku_di" bpmnElement="Task_1omw4ku" bioc:stroke="black">
        <dc:Bounds x="257" y="134" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0bmc6tz_di" bpmnElement="SequenceFlow_0bmc6tz" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="109" />
        <di:waypoint xsi:type="dc:Point" x="307" y="134" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="122" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_0ohg6e3_di" bpmnElement="Task_0ohg6e3" bioc:stroke="black">
        <dc:Bounds x="257" y="244" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0vbongb_di" bpmnElement="SequenceFlow_0vbongb" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="214" />
        <di:waypoint xsi:type="dc:Point" x="307" y="244" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="229" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_0q0cw3u_di" bpmnElement="Task_0q0cw3u" bioc:stroke="black">
        <dc:Bounds x="257" y="655" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0xzg1jf_di" bpmnElement="Task_0xzg1jf" bioc:stroke="black">
        <dc:Bounds x="257" y="354" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0xa1iwl_di" bpmnElement="SequenceFlow_0xa1iwl" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="324" />
        <di:waypoint xsi:type="dc:Point" x="307" y="354" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="339" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_0czf8dr_di" bpmnElement="Task_0czf8dr" bioc:stroke="blue">
        <dc:Bounds x="257" y="464" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_13o5lwi_di" bpmnElement="SequenceFlow_13o5lwi" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="307" y="434" />
        <di:waypoint xsi:type="dc:Point" x="307" y="464" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="449" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="ExclusiveGateway_11vfkka_di" bpmnElement="ExclusiveGateway_11vfkka" isMarkerVisible="true" bioc:stroke="blue">
        <dc:Bounds x="282" y="574" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="339" y="592" width="58" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_00ib3gt_di" bpmnElement="SequenceFlow_00ib3gt" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="307" y="544" />
        <di:waypoint xsi:type="dc:Point" x="307" y="574" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="559" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0u0a7y4_di" bpmnElement="SequenceFlow_0u0a7y4" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="307" y="624" />
        <di:waypoint xsi:type="dc:Point" x="307" y="655" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="312" y="626" width="12" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0lzglr0_di" bpmnElement="SequenceFlow_0lzglr0" bioc:stroke="blue">
        <di:waypoint xsi:type="dc:Point" x="282" y="599" />
        <di:waypoint xsi:type="dc:Point" x="227" y="599" />
        <di:waypoint xsi:type="dc:Point" x="227" y="599" />
        <di:waypoint xsi:type="dc:Point" x="172" y="599" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="219" y="579" width="21" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="EndEvent_07csksb_di" bpmnElement="EndEvent_0iyib5c" bioc:stroke="blue">
        <dc:Bounds x="136" y="581" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="111" y="617" width="86" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0chnlrf_di" bpmnElement="Task_0chnlrf" bioc:stroke="black">
        <dc:Bounds x="257" y="765" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0a4t20v_di" bpmnElement="SequenceFlow_0a4t20v" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="735" />
        <di:waypoint xsi:type="dc:Point" x="307" y="765" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="750" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_19mdcmi_di" bpmnElement="SequenceFlow_19mdcmi" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="845" />
        <di:waypoint xsi:type="dc:Point" x="307" y="875" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="860" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_1bovh02_di" bpmnElement="Task_1bovh02" bioc:stroke="black">
        <dc:Bounds x="257" y="1004" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_040cv74_di" bpmnElement="SequenceFlow_040cv74" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="911" />
        <di:waypoint xsi:type="dc:Point" x="307" y="1004" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="958" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="EndEvent_0c9infk_di" bpmnElement="EndEvent_0c9infk" bioc:stroke="black">
        <dc:Bounds x="289" y="1115" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="266" y="1151" width="83" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1sz6ye3_di" bpmnElement="SequenceFlow_1sz6ye3" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="307" y="1084" />
        <di:waypoint xsi:type="dc:Point" x="307" y="1115" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="277" y="1100" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="IntermediateCatchEvent_0wt4ekn_di" bpmnElement="IntermediateThrowEvent_0gcgzel" bioc:stroke="black">
        <dc:Bounds x="289" y="875" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="223" y="881" width="55" height="24" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0fpi4xt_di" bpmnElement="Task_0fpi4xt" bioc:stroke="red">
        <dc:Bounds x="485" y="556" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="Task_0jkktt6_di" bpmnElement="Task_0jkktt6" bioc:stroke="green">
        <dc:Bounds x="569" y="914" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_07zq1sx_di" bpmnElement="ExclusiveGateway_07zq1sx" isMarkerVisible="true" bioc:stroke="green">
        <dc:Bounds x="594" y="1019" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="645" y="1048" width="61" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1squaca_di" bpmnElement="SequenceFlow_1squaca" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="619" y="994" />
        <di:waypoint xsi:type="dc:Point" x="619" y="1019" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="589" y="1007" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_106y2vc_di" bpmnElement="Task_106y2vc" bioc:stroke="fuchsia">
        <dc:Bounds x="949" y="914" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_1lodnfn_di" bpmnElement="ExclusiveGateway_1lodnfn" isMarkerVisible="true" bioc:stroke="fuchsia">
        <dc:Bounds x="974" y="1052" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="967" y="1102" width="65" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0nppu2d_di" bpmnElement="SequenceFlow_0nppu2d" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="999" y="994" />
        <di:waypoint xsi:type="dc:Point" x="999" y="1052" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="969" y="1023" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1r7gzma_di" bpmnElement="SequenceFlow_1r7gzma" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="975" y="1076" />
        <di:waypoint xsi:type="dc:Point" x="867" y="1076" />
        <di:waypoint xsi:type="dc:Point" x="355" y="1076" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="919" y="1057" width="21" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_0rpafiw_di" bpmnElement="Task_0rpafiw" bioc:stroke="fuchsia">
        <dc:Bounds x="1094" y="1037" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_13iuw1h_di" bpmnElement="SequenceFlow_13iuw1h" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="1024" y="1077" />
        <di:waypoint xsi:type="dc:Point" x="1064" y="1077" />
        <di:waypoint xsi:type="dc:Point" x="1064" y="1077" />
        <di:waypoint xsi:type="dc:Point" x="1094" y="1077" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1044" y="1054" width="10" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_15c0efz_di" bpmnElement="Task_15c0efz" bioc:stroke="fuchsia">
        <dc:Bounds x="1094" y="1144" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0go5rf4_di" bpmnElement="SequenceFlow_0go5rf4" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="1144" y="1117" />
        <di:waypoint xsi:type="dc:Point" x="1144" y="1144" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1114" y="1131" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="EndEvent_0zjjaeu_di" bpmnElement="EndEvent_0zjjaeu" bioc:stroke="fuchsia">
        <dc:Bounds x="1126" y="1259" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1103" y="1295" width="89" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_1iu0x65_di" bpmnElement="SequenceFlow_1iu0x65" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="1144" y="1224" />
        <di:waypoint xsi:type="dc:Point" x="1144" y="1259" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="1114" y="1242" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="Task_1n1e5yk_di" bpmnElement="Task_1n1e5yk" bioc:stroke="red">
        <dc:Bounds x="485" y="448" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_0hxlup9_di" bpmnElement="SequenceFlow_0hxlup9" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="535" y="528" />
        <di:waypoint xsi:type="dc:Point" x="535" y="556" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="505" y="542" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0hpvyjm_di" bpmnElement="SequenceFlow_0hpvyjm" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="535" y="636" />
        <di:waypoint xsi:type="dc:Point" x="535" y="805" />
        <di:waypoint xsi:type="dc:Point" x="357" y="805" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="505" y="721" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0hmi9dn_di" bpmnElement="SequenceFlow_0hmi9dn" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="357" y="394" />
        <di:waypoint xsi:type="dc:Point" x="535" y="394" />
        <di:waypoint xsi:type="dc:Point" x="535" y="448" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="401" y="379" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_09e7uxu_di" bpmnElement="SequenceFlow_09e7uxu" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="325" y="893" />
        <di:waypoint xsi:type="dc:Point" x="619" y="893" />
        <di:waypoint xsi:type="dc:Point" x="619" y="914" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="427" y="878" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1h3ntrj_di" bpmnElement="SequenceFlow_1h3ntrj" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="594" y="1044" />
        <di:waypoint xsi:type="dc:Point" x="439" y="1044" />
        <di:waypoint xsi:type="dc:Point" x="357" y="1044" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="449" y="1044" width="21" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_05l0ut4_di" bpmnElement="SequenceFlow_05l0ut4" bioc:stroke="green">
        <di:waypoint xsi:type="dc:Point" x="619" y="1069" />
        <di:waypoint xsi:type="dc:Point" x="619" y="1184" />
        <di:waypoint xsi:type="dc:Point" x="181" y="1184" />
        <di:waypoint xsi:type="dc:Point" x="181" y="695" />
        <di:waypoint xsi:type="dc:Point" x="257" y="695" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="398" y="1169" width="10" height="12" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0hzy7k8_di" bpmnElement="SequenceFlow_0hzy7k8" bioc:stroke="fuchsia">
        <di:waypoint xsi:type="dc:Point" x="325" y="893" />
        <di:waypoint xsi:type="dc:Point" x="999" y="893" />
        <di:waypoint xsi:type="dc:Point" x="999" y="914" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="617" y="878" width="90" height="0" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_1xz27vz_di" bpmnElement="TextAnnotation_1xz27vz" bioc:stroke="black">
        <dc:Bounds x="639" y="425" width="147" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_12jwgy7_di" bpmnElement="Association_12jwgy7" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="585" y="475" />
        <di:waypoint xsi:type="dc:Point" x="657" y="455" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_0t71a3a_di" bpmnElement="TextAnnotation_0t71a3a" bioc:stroke="black">
        <dc:Bounds x="635" y="549" width="147" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_1h5m3ew_di" bpmnElement="Association_1h5m3ew" bioc:stroke="red">
        <di:waypoint xsi:type="dc:Point" x="585" y="581" />
        <di:waypoint xsi:type="dc:Point" x="635" y="566" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_1d9slmw_di" bpmnElement="TextAnnotation_1d9slmw" bioc:stroke="black">
        <dc:Bounds x="80" y="441" width="189" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_09768cp_di" bpmnElement="Association_09768cp" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="257" y="497" />
        <di:waypoint xsi:type="dc:Point" x="82" y="472" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_01sih2u_di" bpmnElement="TextAnnotation_01sih2u" bioc:stroke="black">
        <dc:Bounds x="1104" y="901" width="182" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_1u2lasa_di" bpmnElement="Association_1u2lasa" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1049" y="944" />
        <di:waypoint xsi:type="dc:Point" x="1118" y="931" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_1b8lq1s_di" bpmnElement="TextAnnotation_1b8lq1s" bioc:stroke="black">
        <dc:Bounds x="694" y="922" width="192" height="34" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_03ctd2m_di" bpmnElement="Association_03ctd2m" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="669" y="950" />
        <di:waypoint xsi:type="dc:Point" x="694" y="947" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_1x0b7uw_di" bpmnElement="TextAnnotation_1x0b7uw" bioc:stroke="black">
        <dc:Bounds x="1219" y="1037" width="211" height="31" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_01evohe_di" bpmnElement="Association_01evohe" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1194" y="1070" />
        <di:waypoint xsi:type="dc:Point" x="1219" y="1067" />
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNShape id="TextAnnotation_05j2p0k_di" bpmnElement="TextAnnotation_05j2p0k" bioc:stroke="black">
        <dc:Bounds x="1219" y="1134" width="185" height="30" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="Association_0a9n6um_di" bpmnElement="Association_0a9n6um" bioc:stroke="black">
        <di:waypoint xsi:type="dc:Point" x="1194" y="1174" />
        <di:waypoint xsi:type="dc:Point" x="1240" y="1164" />
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>

`
