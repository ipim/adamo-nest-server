const content_en = `
        <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td width="10%" valign="top">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td width="10%" valign="top">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>DESCRIPTION&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td width="10%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="194 144 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0vfye2t" transform="matrix(1, 0, 0, 1, 200, 150)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            <td width="10%" valign="top">
                <p>Task</p>
            </td>
            <td width="40%" valign="top">
                <p>A Task is <strong>work</strong> that is <strong>performed</strong> within a Business Process</p>
                <ul type="disc">
                <li>it can be <strong>atomic</strong> or <strong>non-atomic</strong> (compound)</li>
                <li><strong>represent</strong> <strong>points</strong> in a Process flow where <strong>work</strong> is <strong>performed</strong></li>
                <li>is a <strong>executable</strong> <strong>element</strong> of a BPMN</li>
                </ul>
            </td>
            <td width="40%" valign="top">
                <p><strong>A activity can be:</strong></p>
                <p><strong>Task</strong></p>
                <ul type="disc">
                <li>an <strong>atomic</strong> Activity within a <strong>Process flow</strong></li>
                <li>is used when the <strong>work</strong> in the Process <strong>cannot</strong> be <strong>broken down</strong> to a <strong>finer level</strong> of detail</li>
                <li>an <strong>end-user</strong> or <strong>applications</strong> are used to <strong>perform</strong> the Task when it is <strong>executed</strong></li>
                </ul>
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="244 34 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_16meoi6" transform="matrix(1, 0, 0, 1, 250, 40)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 5px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1, 0, 0, 1, 42.5, 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" style="fill: white; stroke-width: 2px; stroke: black;" data-marker="sub-process"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
                <p><strong>Sub-Process</strong></p>
                <p><strong>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="304 234 112 92" version="1.1"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1x15bp8" style="display: block;" transform="matrix(1 0 0 1 310 240)"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text lineHeight="1.2" class="djs-label" style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1 0 0 1 42.5 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" data-marker="sub-process" style="fill: white; stroke-width: 2px; stroke: black;"/></g><rect x="-6" y="-6" width="112" height="92" class="djs-outline" style="fill: none;"/><rect class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/></g><g class="djs-children"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_05gvqho" style="display: none;" transform="matrix(1 0 0 1 225 262)"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="48" height="48" class="djs-outline" style="fill: none;"/></g></g></g></g></svg>
                </strong></p>
                <p><strong>Call Activity</strong></p>
                <ul type="disc">
                <li>allows the <strong>inclusion</strong> of <strong>re-usable</strong> Tasks and Processes in the diagram</li>
                </ul>
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="244 34 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_16meoi6" transform="matrix(1, 0, 0, 1, 250, 40)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 5px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1, 0, 0, 1, 42.5, 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" style="fill: white; stroke-width: 2px; stroke: black;" data-marker="sub-process"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`

const content_de = `
        <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td valign="top" width="10%">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td valign="top" width="10%">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BESCHREIBUNG</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="10%">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="194 144 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0vfye2t" transform="matrix(1, 0, 0, 1, 200, 150)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            <td valign="top" width="10%">
                <p>Task</p>
            </td>
            <td valign="top" width="40%">
                <p>Ein Task ist <strong>Arbeit</strong>, die innerhalb eines Geschäftsprozesses <strong>ausgeführt</strong> wird.</p>
                <ul type="disc">
                <li>kann <strong>atomar</strong> oder <strong>nicht atomar&nbsp;</strong>(zusammengesetzt) sein</li>
                <li><strong>repräsentiert Punkte</strong> in einem Prozessablauf, an denen&nbsp;<strong>Arbeit verrichtet&nbsp;</strong>wird</li>
                <li>ist ein <strong>ausführbares Element</strong> eines BPMN</li>
                </ul>
            </td>
            <td valign="top" width="40%">
                <p><strong>Eine Aktivität kann folgendes sein:</strong></p>
                <p><strong>Task</strong></p>
                <ul type="disc">
                <li>eine&nbsp;<strong>atomare</strong> Aktivität innerhalb eines&nbsp;<strong>Prozessablaufs</strong></li>
                <li>wird verwendet, wenn die <strong>Arbeit</strong> im Prozess <strong>nicht</strong> auf eine <strong>feinere Detailstufe</strong> <strong>heruntergebrochen</strong> werden kann</li>
                <li>ein&nbsp;<strong>Endbenutzer</strong> oder&nbsp;<strong>Anwendungen</strong> zur&nbsp;<strong>Ausführung</strong> des Tasks verwendet werden, wenn dieser&nbsp;<strong>ausgeführt&nbsp;</strong>wird</li>
                </ul>
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="244 34 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_16meoi6" transform="matrix(1, 0, 0, 1, 250, 40)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 5px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1, 0, 0, 1, 42.5, 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" style="fill: white; stroke-width: 2px; stroke: black;" data-marker="sub-process"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
                <p><strong>Sub-Process</strong></p>
                <p><strong>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="304 234 112 92" version="1.1"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1x15bp8" style="display: block;" transform="matrix(1 0 0 1 310 240)"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text lineHeight="1.2" class="djs-label" style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1 0 0 1 42.5 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" data-marker="sub-process" style="fill: white; stroke-width: 2px; stroke: black;"/></g><rect x="-6" y="-6" width="112" height="92" class="djs-outline" style="fill: none;"/><rect class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/></g><g class="djs-children"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_05gvqho" style="display: none;" transform="matrix(1 0 0 1 225 262)"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="48" height="48" class="djs-outline" style="fill: none;"/></g></g></g></g></svg>
                </strong></p>
                <p><strong>Call Activity</strong></p>
                <ul type="disc">
                <li>erlaubt die <strong>Aufnahme</strong> <strong>wiederverwendbarer</strong> Tasks und Prozesse in das Diagramm</li>
                </ul>
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="112" height="92" viewBox="244 34 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_16meoi6" transform="matrix(1, 0, 0, 1, 250, 40)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 5px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><rect x="0" y="0" width="14" height="14" rx="0" ry="0" style="stroke: black; stroke-width: 1px; fill: white;" transform="matrix(1, 0, 0, 1, 42.5, 60)"/><path d="m42.5,60 m 7,2 l 0,10 m -5,-5 l 10,0" style="fill: white; stroke-width: 2px; stroke: black;" data-marker="sub-process"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`

export const task= {
    content_de,
    content_en
}
