const content_en =  `
        <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td width="10%" valign="top">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td width="10%" valign="top">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>DESCRIPTION&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td width="10%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-16 104 612 262" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape selected" data-element-id="Participant_1scgqhw" transform="matrix(1, 0, 0, 1, -10, 110)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="600" height="250" rx="0" ry="0" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><polyline points="30,0 30,250 " style="fill: none; stroke: black; stroke-width: 2px;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label" transform="matrix(-1.83697e-16, -1, 1, -1.83697e-16, 0, 250)"><tspan x="125" y="18.6"/></text></g><rect x="-6" y="-6" width="612" height="262" style="fill: none;" class="djs-outline"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-click-stroke" x="0" y="0" width="600" height="250"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="30" height="250"/></g></g></svg>
                </p>
            </td>
            <td width="10%" valign="top">
                <p>Pool</p>
            </td>
            <td width="40%" valign="top">
                <p>A pool is a <strong>limited</strong> <strong>space</strong> of a <strong>single process</strong> (contains the <strong>sequence flows</strong> between <strong>activities</strong>).&nbsp;</p>
                <p>A <strong>process</strong> is <strong>completely</strong> <strong>contained</strong> in the <strong>pool</strong>. There is always <strong>at least one pool</strong> in our process model.</p>
            </td>
            <td width="40%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="154 94 552 172" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="association-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="12" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15" style="fill: none; stroke-width: 1.5px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Participant_0ezcdyr" transform="matrix(1, 0, 0, 1, 160, 100)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="540" height="160" rx="0" ry="0" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><polyline points="30,0 30,160 " style="fill: none; stroke: black; stroke-width: 2px;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label" transform="matrix(-1.83697e-16, -1, 1, -1.83697e-16, 0, 160)"><tspan x="53" y="18.6">Company</tspan></text></g><rect x="-6" y="-6" width="552" height="172" style="fill: none;" class="djs-outline"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-click-stroke" x="0" y="0" width="540" height="160"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="30" height="160"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`;

const content_de = `
        <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td valign="top" width="10%">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td valign="top" width="10%">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BESCHREIBUNG</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="10%">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-16 104 612 262" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape selected" data-element-id="Participant_1scgqhw" transform="matrix(1, 0, 0, 1, -10, 110)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="600" height="250" rx="0" ry="0" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><polyline points="30,0 30,250 " style="fill: none; stroke: black; stroke-width: 2px;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label" transform="matrix(-1.83697e-16, -1, 1, -1.83697e-16, 0, 250)"><tspan x="125" y="18.6"/></text></g><rect x="-6" y="-6" width="612" height="262" style="fill: none;" class="djs-outline"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-click-stroke" x="0" y="0" width="600" height="250"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="30" height="250"/></g></g></svg>
                </p>
            </td>
            <td valign="top" width="10%">
                <p>Pool</p>
            </td>
            <td valign="top" width="40%">
                <p>Ein Pool ist ein <strong>begrenzter Bereich</strong> eines <strong>einzelnen Prozesses</strong> (enthält die <strong>Sequenzflüsse</strong> zwischen den <strong>Aktivitäten</strong>). Ein <strong>Prozess ist vollständig im Pool enthalten</strong>. In unserem Prozessmodell gibt es immer <strong>mindestens einen Pool</strong>.</p>
            </td>
            <td valign="top" width="40%">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="154 94 552 172" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="association-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="12" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15" style="fill: none; stroke-width: 1.5px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Participant_0ezcdyr" transform="matrix(1, 0, 0, 1, 160, 100)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="540" height="160" rx="0" ry="0" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><polyline points="30,0 30,160 " style="fill: none; stroke: black; stroke-width: 2px;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label" transform="matrix(-1.83697e-16, -1, 1, -1.83697e-16, 0, 160)"><tspan x="63.5" y="18.6">Firma</tspan></text></g><rect x="-6" y="-6" width="552" height="172" style="fill: none;" class="djs-outline"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-click-stroke" x="0" y="0" width="540" height="160"/><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="30" height="160"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`;

export const pool = {
    content_de,
    content_en,
};
