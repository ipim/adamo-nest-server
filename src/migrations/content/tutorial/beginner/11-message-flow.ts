const content_en = `
        <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td width="10%" valign="top">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td width="10%" valign="top">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>DESCRIPTION&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td width="10%" valign="top">
                <p>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAAAcCAMAAADofvVKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAByUExURf////7+/u7u7gAAAGtra9LS0vT09F5eXrCwsPDw8ENDQ4aGhurq6ikpKXx8fOPj4w4ODvn5+U9PT/j4+BwcHFVVVcvLyycnJ2dnZ9ra2jw8PJaWlt/f3z8/P7W1tTg4ON7e3v39/RcXF2NjY9vb2wAAAEDgl0YAAAAmdFJOU/////////////////////////////////////////////////8Ap3qBvAAAAAlwSFlzAAAh1QAAIdUBBJy0nQAAAIFJREFUWEft1r0KgCAYhWHRJQyixR8Isvu/yqy+QqO2Bk+cZzJ0eDWJFBEREX1Em7hoGQPSJnOdPMHZ8zPbQ76DMz8LA94OivxNfN2BLGie9RJc8TLbvOAe+9t0uzwhLjKBocxPE1Z7duWnGfHLeeQnuGMX2oxg170G/MNDRP+k1ArtOwd+dquHuQAAAABJRU5ErkJggg==" width="83">
                </p>
            </td>
            <td width="10%" valign="top">
                <p>Sequence Flow</p>
            </td>
            <td width="40%" valign="top">
                <p>The Sequence Flow <strong>represents</strong> the <strong>order</strong> in which all <strong>activities</strong> in the <strong>process</strong> are <strong>executed</strong>.</p>
            </td>
            <td width="40%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="106 64 1008 202" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="association-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="12" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15" style="fill: none; stroke-width: 1.5px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1wke65b" style="display: block;"><g class="djs-visual"><path d="m  148,110L200,110 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="148,110 200,110 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="142" y="104" width="64" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0q6g6uz" style="display: block;"><g class="djs-visual"><path d="m  300,110L355,110 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="300,110 355,110 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="294" y="104" width="67" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_11gclmp" style="display: block;"><g class="djs-visual"><path d="m  405,110L460,110 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="405,110 460,110 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="399" y="104" width="67" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0rhpfng" style="display: block;"><g class="djs-visual"><path d="m  380,135L380,220 L460,220 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="380,135 380,220 460,220 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="374" y="129" width="92" height="97" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1pm8u2h" style="display: block;"><g class="djs-visual"><path d="m  560,110L620,110 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="560,110 620,110 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="554" y="104" width="72" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1q5dkcv" style="display: block;"><g class="djs-visual"><path d="m  720,110L810,110 L810,145 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="720,110 810,110 810,145 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="714" y="104" width="102" height="47" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1a1gwp1" style="display: block;"><g class="djs-visual"><path d="m  560,220L810,220 L810,195 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="560,220 810,220 810,195 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="554" y="189" width="262" height="37" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0h9uyfi" style="display: block;"><g class="djs-visual"><path d="m  835,170L900,170 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="835,170 900,170 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="829" y="164" width="77" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0szue2e" style="display: block;"><g class="djs-visual"><path d="m  1000,170L1072,170 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="1000,170 1072,170 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="994" y="164" width="84" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_1g2szpk" transform="matrix(1, 0, 0, 1, 112, 92)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_09bzctc" transform="matrix(1, 0, 0, 1, 200, 70)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1je1p32" transform="matrix(1, 0, 0, 1, 460, 70)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1xhzsm6" transform="matrix(1, 0, 0, 1, 460, 180)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0imnsxh" transform="matrix(1, 0, 0, 1, 620, 70)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Gateway_0xi4r93" transform="matrix(1, 0, 0, 1, 785, 145)" style="display: block;"><g class="djs-visual"><polygon points="25,0 50,25 25,50 0,25" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><path d="m 23,10 0,12.5 -12.5,0 0,5 12.5,0 0,12.5 5,0 0,-12.5 12.5,0 0,-5 -12.5,0 0,-12.5 -5,0 z" style="fill: black; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="50" height="50"/><rect x="-6" y="-6" width="62" height="62" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Gateway_1j57ffs" transform="matrix(1, 0, 0, 1, 355, 85)" style="display: block;"><g class="djs-visual"><polygon points="25,0 50,25 25,50 0,25" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><path d="m 23,10 0,12.5 -12.5,0 0,5 12.5,0 0,12.5 5,0 0,-12.5 12.5,0 0,-5 -12.5,0 0,-12.5 -5,0 z" style="fill: black; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="50" height="50"/><rect x="-6" y="-6" width="62" height="62" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0wkpos9" transform="matrix(1, 0, 0, 1, 900, 130)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_1l63f7p" transform="matrix(1, 0, 0, 1, 1072, 152)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 4px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`;

const content_de = `
        <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td valign="top" width="10%">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td valign="top" width="10%">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BESCHREIBUNG</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="10%">
                <p>
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAAAZCAMAAAC4s2T5AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFiUExURf///ygoKImJieXl5QAAAJqamv39/Tk5OQsLC/b29uLi4o+Pj19fX4WFhd/f3/7+/oCAgA8PD/Ly8ru7u7m5uff39xMTE4SEhO3t7ezs7AwMDLCwsL+/vwMDAwICAvHx8fn5+Xp6eg4ODu7u7qCgoIyMjMPDwzc3N2RkZKqqquvr63Z2dsfHx9DQ0GZmZm5ubu/v77a2tn19ffv7+5ubm5eXl+bm5nR0dOjo6M7OzmVlZVpaWpKSkhkZGQYGBoODg2NjY/X19RAQEPr6+h4eHkJCQnFxcRYWFtPT0wgICAUFBfT09OPj4xISEpCQkCkpKTo6OtfX15iYmKenp9TU1ElJSZSUlMLCwsnJyZOTk5GRkeHh4bGxsY6OjpycnJmZmY2Njdvb2woKCre3t8/PzwEBAYKCgq6urpaWlvz8/KGhofPz83x8fHNzc9ra2q+vrxoaGnh4eG1tbWtraxQUFAAAAJPXAUcAAAB2dFJOU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wABYqlHAAAACXBIWXMAACHVAAAh1QEEnLSdAAABN0lEQVRYR+2UMa7DIAyGI+Q5S47AEEVI3hFb1JyD+9+iBv8JpE3b6ekRiS8Z7B8JbGM8dDqdzp/xMDBuiSWiCPuGWJolgW83YE0wI+zmsFJ78y2BSRaJJnitMebWMaskYFU5I821WEnhcvH/SdVPuHQDFzHGvO5p5sD7v+iSdUVi9ir6SgqMDadKYqfaEIsUeH9+rkgccOWWaxFbenHkC8fDdekGYBeYct/nDjrIK5JZzariBldhFWXnCk00zYwKjepFzFru7cJDxfRiFfUFOeXtjUZKtfa0xcJefQM/g1L5k4hSLXAze4ngAohXW57PQYQ4xxwVZ8lk36Ug9ZgGv7be+x+nz5Jv6D2vNtDJE6VvPkVo5Tk1PfcXCd412h0/GIkleEyI+5FG1Qb7hth1htXpdO7PMDwB/dlMNAyyuucAAAAASUVORK5CYII=" width="83">
                </p>
            </td>
            <td valign="top" width="10%">
                <p>Message Flow</p>
            </td>
            <td valign="top" width="40%">
                <p>Wird verwendet, um den <strong>Nachrichtenfluss</strong> zwischen <strong>zwei Objekten</strong> <strong>anzuzeigen</strong>, die im Begriff sind Nachrichten zu <strong>senden</strong> und <strong>zu empfangen</strong>.</p>
            </td>
            <td valign="top" width="40%">
                <p>Wird verwendet, um den <strong>Nachrichtenfluss zwischen den einzelnen Pools</strong> und<strong>&nbsp;Lanes</strong> anzuzeigen. Sie können den Nachrichtenfluss <strong>nicht verwenden</strong>, um <strong>Flow-Objekte</strong> <strong>innerhalb desselben Teilnehmers</strong> zu <strong>verbinden</strong>.</p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`;

export const messageFlow = {
    content_de,
    content_en,
};
