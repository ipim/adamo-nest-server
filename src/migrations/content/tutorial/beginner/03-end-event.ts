const content_en = `
                <div>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <style>
                    table, th, td {
                    padding: 15px;
                    }
                </style>
                <tbody>
                    <tr>
                    <td width="10%" valign="top">
                        <p><strong>NOTATION&nbsp;</strong></p>
                    </td>
                    <td width="10%" valign="top">
                        <p><strong>ELEMENT NAME&nbsp;</strong></p>
                    </td>
                    <td width="40%" valign="top">
                        <p><strong>DESCRIPTION&nbsp;</strong></p>
                    </td>
                    <td width="40%" valign="top">
                        <p><strong>BEST PRACTICE&nbsp;</strong></p>
                    </td>
                    </tr>
                    <tr>
                    <td width="10%" valign="top">
                        <p>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="268 186 64 69" version="1.1"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0vg1ktu" style="display: block;" transform="matrix(1 0 0 1 282 192)"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 4px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="48" height="48" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0vg1ktu_label" style="display: block;" transform="matrix(1 0 0 1 274 235)"><g class="djs-visual"><text lineHeight="1.2" class="djs-label" style="font-family: Arial, sans-serif; font-size: 11px; font-weight: normal; fill: black;"><tspan x="0" y="9.899999999999999">End Event</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="52" height="14" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="64" height="26" class="djs-outline" style="fill: none;"/></g></g></svg>
                        </p>
                    </td>
                    <td width="10%" valign="top">
                        <p>End Event</p>
                    </td>
                    <td width="40%" valign="top">
                        <p>Indicates when the Process <strong>ends</strong>.</p>
                        <p>&nbsp;</p>
                        <p>In terms of Sequence Flows, the End Event <strong>ends</strong> the flow of the <strong>Process</strong>, and thus, will <strong>not have</strong> any <strong>outgoing Sequence Flows</strong>.</p>
                        <p>&nbsp;</p>
                        <p>The End Event shares the same basic shape of the Start Event and Intermediate Event, a circle with an open center so that markers can be placed within the circle to indicate variations of the Event.</p>
                        <p>&nbsp;</p>
                        <p><strong>End Event consumes</strong> a <strong>token</strong> that had been <strong>generated</strong> from a <strong>Start</strong> Event within the <strong>same</strong> level of <strong>Process</strong>.</p>
                    </td>
                    <td width="40%" valign="top">
                        <p>End Event <strong>indicates</strong> where a Process will <strong>end</strong>.</p>
                        <p>&nbsp;</p>
                        <p>If <strong>parallel</strong> Sequence Flows <strong>targets</strong> the <strong>End Event</strong>, then the <strong>tokens</strong> will be <strong>consumed</strong> as they <strong>arrive</strong>.</p>
                        <p>All the tokens that were generated within the Process <strong>MUST</strong> be consumed by an End Event <strong>before</strong> the Process has been <strong>completed</strong>.</p>
                        <p>&nbsp;</p>
                        <p>In <strong>exception flow</strong>, if the Process is a Sub-Process, it can be <strong>stopped prior</strong> to <strong>normal</strong> completion through <strong>interrupting Intermediate Events</strong></p>
                        <ul type="disc">
                        <li>In this situation the <strong>tokens</strong> will be <strong>consumed</strong> by an <strong>Intermediate</strong> Event attached to the <strong>boundary</strong> of the <strong>Sub-Process</strong>.</li>
                        </ul>
                        <p>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60%" height="60%" viewBox="276 94 220 212" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0ihmorq" style="display: block;"><g class="djs-visual"><path d="m  320,198L320,260 L390,260 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="320,198 320,260 390,260 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="314" y="192" width="82" height="74" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0nhaq89" transform="matrix(1, 0, 0, 1, 290, 100)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1645169" transform="matrix(1, 0, 0, 1, 390, 220)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_07lyy5l" transform="matrix(1, 0, 0, 1, 302, 162)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 1px; fill: white; fill-opacity: 1;"/><circle cx="18" cy="18" r="15" style="stroke: black; stroke-width: 1px; fill: none;"/><path d="m 7.2,25.991999999999997 0.09350000000000001,-0.025300000000000003 7.3392,-9.610700000000001 7.667000000000001,8.9661 4.7003,-18.2204 -5.8707,11.6501 -7.299600000000001,-9.585400000000002 z" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Flow_0ihmorq_label" transform="matrix(1, 0, 0, 1, 282, 263)" style="display: block;"><g class="djs-visual"><text style="font-family: Arial, sans-serif; font-size: 11px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="0" y="9.899999999999999">Exception Flow</tspan></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="75" height="14"/><rect x="-6" y="-6" width="87" height="26" style="fill: none;" class="djs-outline"/></g></g></svg>
                        </p>
                    </td>
                    </tr>
                </tbody>
                </table>
                </div>`

const content_de = `
                <div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <style>
                table, th, td {
                padding: 15px;}
                </style>
                <tbody>
                    <tr>
                    <td valign="top" width="10%">
                        <p><strong>NOTATION&nbsp;</strong></p>
                    </td>
                    <td valign="top" width="10%">
                        <p><strong>ELEMENT NAME&nbsp;</strong></p>
                    </td>
                    <td valign="top" width="40%">
                        <p><strong>BESCHREIBUNG</strong></p>
                    </td>
                    <td valign="top" width="40%">
                        <p><strong>BEST PRACTICE&nbsp;</strong></p>
                    </td>
                    </tr>
                    <tr>
                    <td valign="top" width="10%">
                        <p>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="268 186 64 69" version="1.1"><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0vg1ktu" style="display: block;" transform="matrix(1 0 0 1 282 192)"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 4px; fill: white; fill-opacity: 0.95;"/></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="48" height="48" class="djs-outline" style="fill: none;"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0vg1ktu_label" style="display: block;" transform="matrix(1 0 0 1 274 235)"><g class="djs-visual"><text lineHeight="1.2" class="djs-label" style="font-family: Arial, sans-serif; font-size: 11px; font-weight: normal; fill: black;"><tspan x="0" y="9.899999999999999">End Event</tspan></text></g><rect class="djs-hit djs-hit-all" x="0" y="0" width="52" height="14" style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;"/><rect x="-6" y="-6" width="64" height="26" class="djs-outline" style="fill: none;"/></g></g></svg>
                        </p>
                    </td>
                    <td valign="top" width="10%">
                        <p>End Event</p>
                    </td>
                    <td valign="top" width="40%">
                        <p>Zeigt an, wann der Prozess <strong>endet</strong>.</p>
                        <p>&nbsp;</p>
                        <p>In Bezug auf Sequenzflüsse <strong>beendet</strong> das End Event den Fluss des <strong>Prozesses</strong> und <strong>hat daher keine ausgehenden Sequenzflüsse</strong>.</p>
                        <p>&nbsp;</p>
                        <p>Das End Event hat die gleiche Grundform wie das Start und Intermediate Event, einen Kreis mit einem offenen Zentrum, so dass innerhalb des Kreises Markierungen platziert werden können, um Variationen des Events anzuzeigen.</p>
                        <p>&nbsp;</p>
                        <p>Das <strong>End Event verbraucht</strong> einen <strong>Token</strong>, welcher aus einem <strong>Start</strong> Event innerhalb der <strong>gleichen</strong> <strong>Prozessebene</strong> <strong>erzeugt</strong> wurde.</p>
                    </td>
                    <td valign="top" width="40%">
                        <p>Das End Event <strong>zeigt an</strong>, wo ein Prozess <strong>endet</strong>.</p>
                        <p>&nbsp;</p>
                        <p>Wenn <strong>parallele</strong> Sequenzflüsse auf das <strong>End Event abzielen</strong>, werden die <strong>Token</strong> bei ihrer <strong>Ankunft verbraucht.</strong></p>
                        <p>&nbsp;</p>
                        <p>Alle Token, die innerhalb des Prozesses erzeugt wurden, <strong>MÜSSEN</strong> von einem End Event verbraucht werden, <strong>bevor</strong> der Prozess <strong>abgeschlossen</strong> ist.</p>
                        <p>&nbsp;</p>
                        <p>Wenn der Prozess ein Subprozess ist, kann er im&nbsp;<strong>Ausnahmefluss</strong> durch die&nbsp;<strong>Unterbrechung</strong> von&nbsp;<strong>Intermediate Events</strong> vor dem&nbsp;<strong>normalen&nbsp;</strong>Abschluss&nbsp;<strong>angehalten</strong> werden.</p>
                        <ul type="disc">
                        <li>In dieser Situation werden die <strong>Token</strong> durch ein <strong>Intermediate</strong> Event an der <strong>Grenze</strong> des <strong>Subprozesses</strong> verbraucht.</li>
                        </ul>
                        <p>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60%" height="60%" viewBox="276 94 220 212" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0ihmorq" style="display: block;"><g class="djs-visual"><path d="m  320,198L320,260 L390,260 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="320,198 320,260 390,260 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="314" y="192" width="82" height="74" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0nhaq89" transform="matrix(1, 0, 0, 1, 290, 100)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_1645169" transform="matrix(1, 0, 0, 1, 390, 220)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_07lyy5l" transform="matrix(1, 0, 0, 1, 302, 162)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 1px; fill: white; fill-opacity: 1;"/><circle cx="18" cy="18" r="15" style="stroke: black; stroke-width: 1px; fill: none;"/><path d="m 7.2,25.991999999999997 0.09350000000000001,-0.025300000000000003 7.3392,-9.610700000000001 7.667000000000001,8.9661 4.7003,-18.2204 -5.8707,11.6501 -7.299600000000001,-9.585400000000002 z" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Flow_0ihmorq_label" transform="matrix(1, 0, 0, 1, 282, 263)" style="display: block;"><g class="djs-visual"><text style="font-family: Arial, sans-serif; font-size: 11px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="0" y="9.899999999999999">Exception Flow</tspan></text></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="75" height="14"/><rect x="-6" y="-6" width="87" height="26" style="fill: none;" class="djs-outline"/></g></g></svg>
                        </p>
                    </td>
                    </tr>
                </tbody>
                </table>
                </div>`

export const endEvent = {
    content_de,
    content_en
}
