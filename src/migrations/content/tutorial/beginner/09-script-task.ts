const content_en = `
        <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td width="10%" valign="top">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td width="10%" valign="top">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>DESCRIPTION&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td width="10%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="174 134 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape selected" data-element-id="Activity_0kly9p0" transform="matrix(1, 0, 0, 1, 180, 140)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><path d="m 15,20 c 9.966553,-6.27276 -8.000926,-7.91932 2.968968,-14.938 l -8.802728,0 c -10.969894,7.01868 6.997585,8.66524 -2.968967,14.938 z m -7,-12 l 5,0 m -4.5,3 l 4.5,0 m -3,3 l 5,0m -4,3 l 5,0" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            <td width="10%" valign="top">
                <p>Script Task</p>
            </td>
            <td width="40%" valign="top">
                <p>It is <strong>executed</strong> by a <strong>business process engine</strong>.</p>
                <p>A <strong>modeler</strong> or <strong>implementer</strong> <strong>defines</strong> a <strong>script</strong> in a <strong>language</strong> that the <strong>engine</strong> can <strong>interpret</strong>.</p>
                <p>When the <strong>Task</strong> is <strong>ready</strong> to <strong>start</strong>, the <strong>engine</strong> will <strong>execute</strong> the <strong>script</strong>.</p>
                <p>When the <strong>script</strong> is <strong>completed</strong>, the <strong>Task</strong> will also be <strong>completed</strong>.</p>
            </td>
            <td width="40%" valign="top">
                <p>&nbsp;
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70%" height="70%" viewBox="146 114 308 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="association-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="12" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15" style="fill: none; stroke-width: 1.5px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0t60zdz" style="display: block;"><g class="djs-visual"><path d="m  350,160L412,160 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="350,160 412,160 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="344" y="154" width="74" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1lp0hhf" style="display: block;"><g class="djs-visual"><path d="m  188,160L250,160 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="188,160 250,160 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="182" y="154" width="74" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0ua4r1s" transform="matrix(1, 0, 0, 1, 152, 142)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_125fztt" transform="matrix(1, 0, 0, 1, 412, 142)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 4px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_15kp7gh" transform="matrix(1, 0, 0, 1, 250, 120)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="6" y="43.599999999999994">Filter by amount</tspan></text><path d="m 15,20 c 9.966553,-6.27276 -8.000926,-7.91932 2.968968,-14.938 l -8.802728,0 c -10.969894,7.01868 6.997585,8.66524 -2.968967,14.938 z m -7,-12 l 5,0 m -4.5,3 l 4.5,0 m -3,3 l 5,0m -4,3 l 5,0" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`;

const content_de = `
    <div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <style>
    table, th, td {
    padding: 15px;}
</style>
<tbody>
<tr>
<td valign="top" width="10%">
    <p><strong>NOTATION&nbsp;</strong></p>
</td>
<td valign="top" width="10%">
    <p><strong>ELEMENT NAME&nbsp;</strong></p>
</td>
<td valign="top" width="40%">
    <p><strong>BESCHREIBUNG&nbsp;</strong></p>
</td>
<td valign="top" width="40%">
    <p><strong>BEST PRACTICE&nbsp;</strong></p>
</td>
</tr>
<tr>
<td valign="top" width="10%">
    <p>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="174 134 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape selected" data-element-id="Activity_0kly9p0" transform="matrix(1, 0, 0, 1, 180, 140)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><path d="m 15,20 c 9.966553,-6.27276 -8.000926,-7.91932 2.968968,-14.938 l -8.802728,0 c -10.969894,7.01868 6.997585,8.66524 -2.968967,14.938 z m -7,-12 l 5,0 m -4.5,3 l 4.5,0 m -3,3 l 5,0m -4,3 l 5,0" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" style="fill: none;" class="djs-outline"/></g></g></svg>
    </p>
    </td>
    <td valign="top" width="10%">
    <p>Script Task</p>
</td>
<td valign="top" width="40%">
    <p>Der Task wird von einer <strong>Geschäftsprozess-Engine ausgeführt</strong>. Ein <strong>Modellierer</strong> oder <strong>Implementierer definiert</strong> ein <strong>Skript</strong> in einer <strong>Sprache</strong>, die die <strong>Engine interpretieren</strong> kann. Wenn der <strong>Task startbereit</strong> ist, <strong>führt</strong> die <strong>Engine</strong> das <strong>Skript aus</strong>. Wenn das <strong>Skript abgeschlossen</strong> ist, wird auch der <strong>Task abgeschlossen</strong>.</p>
</td>
<td valign="top" width="40%">
    <p>&nbsp;
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70%" height="70%" viewBox="146 114 308 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="association-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="12" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15" style="fill: none; stroke-width: 1.5px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_0t60zdz" style="display: block;"><g class="djs-visual"><path d="m  350,160L412,160 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="350,160 412,160 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="344" y="154" width="74" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-connection" data-element-id="Flow_1lp0hhf" style="display: block;"><g class="djs-visual"><path d="m  188,160L250,160 " style="fill: none; stroke-width: 2px; stroke: black; stroke-linejoin: round; marker-end: url('#sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog');"/></g><polyline points="188,160 250,160 " style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-stroke"/><rect x="182" y="154" width="74" height="12" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_0ua4r1s" transform="matrix(1, 0, 0, 1, 152, 142)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Event_125fztt" transform="matrix(1, 0, 0, 1, 412, 142)" style="display: block;"><g class="djs-visual"><circle cx="18" cy="18" r="18" style="stroke: black; stroke-width: 4px; fill: white; fill-opacity: 0.95;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="36" height="36"/><rect x="-6" y="-6" width="48" height="48" style="fill: none;" class="djs-outline"/></g></g><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_15kp7gh" transform="matrix(1, 0, 0, 1, 250, 120)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="15" y="36.4">Nach Menge </tspan><tspan x="25" y="50.8">sortieren</tspan></text><path d="m 15,20 c 9.966553,-6.27276 -8.000926,-7.91932 2.968968,-14.938 l -8.802728,0 c -10.969894,7.01868 6.997585,8.66524 -2.968967,14.938 z m -7,-12 l 5,0 m -4.5,3 l 4.5,0 m -3,3 l 5,0m -4,3 l 5,0" style="fill: none; stroke-width: 1px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
    </p>
    </td>
    </tr>
    </tbody>
    </table>
    </div>`;

export const scriptTask = {
    content_de,
    content_en,
};
