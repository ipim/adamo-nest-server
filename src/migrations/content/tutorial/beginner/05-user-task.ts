const content_en =  `
        <div>
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td width="10%" valign="top">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td width="10%" valign="top">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>DESCRIPTION&nbsp;</strong></p>
            </td>
            <td width="40%" valign="top">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td width="10%" valign="top">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="174 134 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0kly9p0" transform="matrix(1, 0, 0, 1, 180, 140)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><path d="m 15,12 c 0.909,-0.845 1.594,-2.049 1.594,-3.385 0,-2.554 -1.805,-4.62199999 -4.357,-4.62199999 -2.55199998,0 -4.28799998,2.06799999 -4.28799998,4.62199999 0,1.348 0.974,2.562 1.89599998,3.405 -0.52899998,0.187 -5.669,2.097 -5.794,4.7560005 v 6.718 h 17 v -6.718 c 0,-2.2980005 -5.5279996,-4.5950005 -6.0509996,-4.7760005 zm -8,6 l 0,5.5 m 11,0 l 0,-5" style="fill: white; stroke-width: 0.5px; stroke: black;"/><path d="m 15,12 m 2.162,1.009 c 0,2.4470005 -2.158,4.4310005 -4.821,4.4310005 -2.66499998,0 -4.822,-1.981 -4.822,-4.4310005 " style="fill: white; stroke-width: 0.5px; stroke: black;"/><path d="m 15,12 m -6.9,-3.80 c 0,0 2.25099998,-2.358 4.27399998,-1.177 2.024,1.181 4.221,1.537 4.124,0.965 -0.098,-0.57 -0.117,-3.79099999 -4.191,-4.13599999 -3.57499998,0.001 -4.20799998,3.36699999 -4.20699998,4.34799999 z" style="fill: black; stroke-width: 0.5px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            <td width="10%" valign="top">
                <p>User Task</p>
            </td>
            <td width="40%" valign="top">
                <p>Is a typical “<strong>workflow</strong>” Task where a <strong>human performer</strong> performs the Task with the <strong>assistance</strong> of a <strong>software application</strong>&nbsp;</p>
                <p><strong>Examples</strong>:</p>
                <ul>
                <li>Phone operator updates customer record</li>
                <li>User changing their password</li>
                </ul>
            </td>
            <td width="40%" valign="top">
                <p>The task is <strong>scheduled</strong> through a <strong>task list</strong> <strong>manager</strong> of some sort</p>
                <p><strong>Examples</strong>:</p>
                <ul type="disc">
                <li>Updating customers master data</li>
                <li>User changing password of customer account</li>
                </ul>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`

const content_de = `
        <div>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <style>
        table, th, td {
        padding: 15px;}
        </style>
        <tbody>
            <tr>
            <td valign="top" width="10%">
                <p><strong>NOTATION&nbsp;</strong></p>
            </td>
            <td valign="top" width="10%">
                <p><strong>ELEMENT NAME&nbsp;</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BESCHREIBUNG</strong></p>
            </td>
            <td valign="top" width="40%">
                <p><strong>BEST PRACTICE&nbsp;</strong></p>
            </td>
            </tr>
            <tr>
            <td valign="top" width="10%">
                <p>
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="174 134 112 92" version="1.1"><defs><marker id="sequenceflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="11" refY="10" markerWidth="10" markerHeight="10" orient="auto"><path d="M 1 5 L 11 10 L 1 15 Z" style="fill: black; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-end-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="8.5" refY="5" markerWidth="20" markerHeight="20" orient="auto"><path d="m 1 5 l 0 -3 l 7 3 l -7 3 z" style="fill: white; stroke-width: 1px; stroke-linecap: butt; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker><marker id="messageflow-start-white-black-27cahvklw56q3f0y0qdbjzmog" viewBox="0 0 20 20" refX="6" refY="6" markerWidth="20" markerHeight="20" orient="auto"><circle cx="6" cy="6" r="3.5" style="fill: white; stroke-width: 1px; stroke-linecap: round; stroke-dasharray: 10000px, 1px; stroke: black;"/></marker></defs><g class="djs-group"><g class="djs-element djs-shape" data-element-id="Activity_0kly9p0" transform="matrix(1, 0, 0, 1, 180, 140)" style="display: block;"><g class="djs-visual"><rect x="0" y="0" width="100" height="80" rx="10" ry="10" style="stroke: black; stroke-width: 2px; fill: white; fill-opacity: 0.95;"/><text style="font-family: Arial, sans-serif; font-size: 12px; font-weight: normal; fill: black;" lineHeight="1.2" class="djs-label"><tspan x="50" y="43.599999999999994"/></text><path d="m 15,12 c 0.909,-0.845 1.594,-2.049 1.594,-3.385 0,-2.554 -1.805,-4.62199999 -4.357,-4.62199999 -2.55199998,0 -4.28799998,2.06799999 -4.28799998,4.62199999 0,1.348 0.974,2.562 1.89599998,3.405 -0.52899998,0.187 -5.669,2.097 -5.794,4.7560005 v 6.718 h 17 v -6.718 c 0,-2.2980005 -5.5279996,-4.5950005 -6.0509996,-4.7760005 zm -8,6 l 0,5.5 m 11,0 l 0,-5" style="fill: white; stroke-width: 0.5px; stroke: black;"/><path d="m 15,12 m 2.162,1.009 c 0,2.4470005 -2.158,4.4310005 -4.821,4.4310005 -2.66499998,0 -4.822,-1.981 -4.822,-4.4310005 " style="fill: white; stroke-width: 0.5px; stroke: black;"/><path d="m 15,12 m -6.9,-3.80 c 0,0 2.25099998,-2.358 4.27399998,-1.177 2.024,1.181 4.221,1.537 4.124,0.965 -0.098,-0.57 -0.117,-3.79099999 -4.191,-4.13599999 -3.57499998,0.001 -4.20799998,3.36699999 -4.20699998,4.34799999 z" style="fill: black; stroke-width: 0.5px; stroke: black;"/></g><rect style="fill: none; stroke-opacity: 0; stroke: white; stroke-width: 15px;" class="djs-hit djs-hit-all" x="0" y="0" width="100" height="80"/><rect x="-6" y="-6" width="112" height="92" style="fill: none;" class="djs-outline"/></g></g></svg>
                </p>
            </td>
            <td valign="top" width="10%">
                <p>User Task</p>
            </td>
            <td valign="top" width="40%">
                <p>Ist ein typischer „<strong>Workflow</strong>“-Task, bei dem eine&nbsp;<strong>Person</strong> die Aufgabe mit&nbsp;<strong>Hilfe einer Softwareanwendung</strong> ausführt.&nbsp;</p>
                <p><strong>Beispiele</strong>:</p>
                <ul>
                <li>Telefonanbieter aktualisiert den Kundendatensatz</li>
                <li>Benutzer, der sein Passwort ändert</li>
                </ul>
            </td>
            <td valign="top" width="40%">
                <p>Der Task wird durch eine Art <strong>Aufgabenlisten-Manager</strong> <strong>geplant</strong></p>
                <p><strong>Beispiele</strong>:</p>
                <ul type="disc">
                <li>Aktualisierung von Kundenstammdaten</li>
                <li>Benutzer, der das Passwort des Kundenkontos ändert</li>
                </ul>
            </td>
            </tr>
        </tbody>
        </table>
        </div>`

export const userTask= {
    content_de,
    content_en
}
