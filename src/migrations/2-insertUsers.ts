import { MigrationInterface, QueryRunner} from 'typeorm';
import { UserEntity } from '../modules/user/user.entity';
import { RoleType } from './../common/constants/role-type';

export class InsertUsers1565611653969 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {

        console.log("2 - Insert Users");
        await queryRunner.manager
            .createQueryBuilder()
            .insert()
            .into(UserEntity)
            .values([
                {
                    email: 'demo@adamo.de',
                    password: '12345678',
                }, {
                    //!Important rename to adaministrator
                    email: 'admin@adamo.de',
                    password: '12345678',
                    role: RoleType.ADMIN,
                },
                ...this.createDummyUsers(20)
            ])
            .execute();
    }
    public createDummyUsers(count : number) {
       let dummyUsers: any = new Array();
        for (let i = 0; i < count ; i++) {
            dummyUsers.push({
                email: `user${i}@adamo.de`,
                password: `user${i}` ,
            })
        }
        return dummyUsers
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "users"`);
    }
}
