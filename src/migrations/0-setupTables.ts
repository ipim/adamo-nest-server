import { MigrationInterface, QueryRunner } from 'typeorm';

export class SetupTask1565611653967 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<any> {
        
        console.log("1 - Setup Tables");
        // await this.down(queryRunner);
        await queryRunner.query(
            `CREATE TABLE "models" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "model_name" character varying NOT NULL, "model_xml" character varying NOT NULL, "model_version" integer NOT NULL, CONSTRAINT "UQ_28d23f5ebf773368621b7cfa91d" UNIQUE ("model_name"), CONSTRAINT "PK_ef9ed7160ea69013636466bf2d5" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE TABLE "permissions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "model_id" uuid NOT NULL, "user_id" uuid NOT NULL, "role_id" uuid NOT NULL, CONSTRAINT "PK_5782485e6ad1e46edc8b1413787" PRIMARY KEY ("id"))`,
        );
        // await queryRunner.query(`CREATE TABLE "permissions" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "permission_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "model_id" uuid NOT NULL, "user_id" character varying NOT NULL, "role_id" uuid NOT NULL, CONSTRAINT "PK_5782485e6ad1e46edc8b1413787" PRIMARY KEY ("id", "permission_id"))`);
        await queryRunner.query(
            `CREATE TYPE "users_role_enum" AS ENUM('USER', 'ADMIN')`,
        );

        await queryRunner.query(
            `CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "first_name" character varying, "last_name" character varying, "role" "users_role_enum" NOT NULL DEFAULT 'USER', "email" character varying, "password" character varying, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE TABLE "role" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "role_name" character varying NOT NULL, "can_read" boolean NOT NULL, "can_write" boolean NOT NULL, "is_admin" boolean NOT NULL, CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY ("id"))`,
        );

        // await queryRunner.query(`CREATE TABLE "models" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "model_name" character varying NOT NULL, "model_xml" character varying NOT NULL, "model_version" integer NOT NULL, CONSTRAINT "UQ_28d23f5ebf773368621b7cfa91d" UNIQUE ("model_name"), CONSTRAINT "UQ_a6a29626d324b33c46efbb9c50b" UNIQUE ("model_version"), CONSTRAINT "PK_ef9ed7160ea69013636466bf2d5" PRIMARY KEY ("id"))`);
        // await queryRunner.query(`CREATE TABLE "role" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "role_name" character varying NOT NULL, "can_read" boolean NOT NULL, "can_write" boolean NOT NULL, "is_admin" boolean NOT NULL, CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY ("id"))`);
        // await queryRunner.query(`ALTER TABLE "models" DROP CONSTRAINT "UQ_28d23f5ebf773368621b7cfa91d"`);
        // await queryRunner.query(`ALTER TABLE "models" DROP CONSTRAINT "UQ_a6a29626d324b33c46efbb9c50b"`);
        await queryRunner.query(
            `ALTER TABLE "permissions" ADD CONSTRAINT "FK_6d66eea6545b90ff0738712c0c4" FOREIGN KEY ("model_id") REFERENCES "models"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "permissions" ADD CONSTRAINT "FK_0a4668147d9d73da5e94feeec62" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "permissions" ADD CONSTRAINT "FK_0a6c60695c3db85b49dc2149a74" FOREIGN KEY ("role_id") REFERENCES "role"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "permissions"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TYPE "users_role_enum"`);
        await queryRunner.query(`DROP TABLE "role"`);
        await queryRunner.query(`DROP TABLE "models"`);
    }
}
