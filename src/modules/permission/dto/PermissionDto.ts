'use strict';

import { ApiPropertyOptional } from '@nestjs/swagger';

import { PermissionEntity } from '../permission.entity';
import { AbstractDto } from '../../../common/dto/AbstractDto';
import { ModelEntity } from '../../../modules/model/model.entity';
import { UserEntity } from '../../../modules/user/user.entity';
import { RoleEntity } from '../../../modules/role/role.entity';

export class PermissionDto extends AbstractDto {
    @ApiPropertyOptional()
    model: ModelEntity;

    @ApiPropertyOptional()
    user: UserEntity;

    @ApiPropertyOptional()
    role: RoleEntity;

    // @ApiPropertyOptional()
    // public permissionID: string;

    constructor(permission: PermissionEntity) {
        super(permission);
        this.model = permission.model;
        this.user = permission.user;
        this.role = permission.role;
    }
}
