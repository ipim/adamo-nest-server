// import { Test, TestingModule } from '@nestjs/testing';
// import { PermissionController } from './permission.controller';
// import { PermissionService } from './permission.service';
// import { PermissionEntity } from './permission.entity';
// import { plainToClass } from 'class-transformer';
// import { ModelEntity } from '../model/model.entity';
// import { UserEntity } from '../user/user.entity';
// import { RoleType } from '../../constants/role-type';
// import { RoleEntity } from '../role/role.entity';

// const testCat3 = 'Test Permission 3';

// describe('PermissionController', () => {
//     let controller: PermissionController;

//     const modelEntity1 = new ModelEntity('1', 'model', 'modelxml', 1);
//     const userEntity1 = new UserEntity('User1', 'LastName', 'UserName', RoleType.User, 'EMailAdresse', '1234');
//     const roleEntity1 = new RoleEntity('1', 'user');
//     const modelEntity2 = new ModelEntity('2', 'model', 'modelxml', 1);
//     const userEntity2 = new UserEntity('User2', 'LastName', 'UserName', RoleType.User, 'EMailAdresse', '1234');
//     const roleEntity2 = new RoleEntity('2', 'user');

//     const modelEntity3 = new ModelEntity('3', 'model', 'modelxml', 1);
//     const userEntity3 = new UserEntity('User3', 'LastName', 'UserName', RoleType.User, 'EMailAdresse', '1234');
//     const roleEntity3 = new RoleEntity('3', 'user');

//     const permission1 = new PermissionEntity(modelEntity1, userEntity1, roleEntity1);
//     const permission2 = new PermissionEntity(modelEntity2, userEntity2, roleEntity2);

//     const permission3 = new PermissionEntity(modelEntity2, userEntity2, roleEntity2);

//     beforeEach(async () => {
//         const module: TestingModule = await Test.createTestingModule({
//             controllers: [PermissionController],
//             providers: [
//                 {
//                     provide: PermissionService,
//                     useValue: {
//                         find: jest
//                             .fn()
//                             .mockReturnValue([
//                                 permission1, permission2,
//                             ]),
//                         create: jest
//                             .fn()
//                             .mockReturnValue(plainToClass(PermissionEntity, permission3)),
//                         updateOne:
//                             jest
//                                 .fn()
//                                 .mockReturnValue(new PermissionEntity(modelEntity2, userEntity2, roleEntity2)),
//                         deleteOne:
//                             jest.fn().mockReturnValue(true),
//                     },
//                 }
//             ],
//         }).compile();
//         controller = module.get<PermissionController>(PermissionController);
//     });

//     /**
//      * These all may seem like simple tests that don't do much, but in reality
//      * the controller itself is pretty simple. Call a service and return it's value,
//      * the complicated stuff comes in either in the service, a pipe, or the interceptor
//      */
//     it('should be defined', () => {
//         expect(controller).toBeDefined();
//     });
//     describe('list all Permissions - listPermissions() ', () => {
//         it('should get the list of cats', async () => {
//             const permissions = await controller.listModels();
//             expect(typeof permissions).toBe('object');
//             expect(permissions[0].model).toBe(modelEntity1);
//             expect(permissions[1].model).toBe(modelEntity2);
//             expect(permissions.length).toBe(2);
//         });
//     });
//     describe('create Permission - create', () => {
//         it('should return a new permission', async () => {
//             const permission = await controller.create(permission3);
//             expect(permission.model).toBe(modelEntity3);
//             expect(permission.permissionName).toBe('TestPermission1');
//         });
//     });
//     describe('update Permission - update()', () => {
//         // it('should get the permission matching the id', () => {
//         //   const retCat = controller.(1);
//         //   expect(typeof retCat).toBe('object');
//         //   expect(retCat.id).toBe(1);
//         //   expect(retCat.name).toBe(testCat1);
//         // });
//     });
//     // describe('deleteCat', () => {
//     //   it('should return true that there was a deletion', () => {
//     //     const delReturn = controller.deleteCat(2);
//     //     expect(typeof delReturn).toBe('boolean');
//     //     expect(delReturn);
//     //   });
//     // });
// });
