// import { PermissionEntity } from './permission.entity';
// import { ModelEntity } from '../model/model.entity';
// import { UserEntity } from '../user/user.entity';
// import { RoleEntity } from '../role/role.entity';
// import { RoleType } from './../../constants/role-type';
// import { PermissionDto } from './dto/PermissionDto';

// describe ('PermissionEntity Class', () => {

//     const modelEntity = new ModelEntity('1', 'model', 'modelxml', 1);
//     const userEntity = new UserEntity('FirstName', 'LastName', 'UserName', RoleType.User, 'EMailAdresse', '1234');
//     const roleEntity = new RoleEntity('1', 'user');

//     it ('should make a permission', () => {
//         const permission = new PermissionEntity(modelEntity, userEntity, roleEntity);
//         expect(permission).toBeTruthy();
//         expect(permission.model).toBe(modelEntity);
//         expect(permission.user).toBe(userEntity);
//         expect(permission.role).toBe(roleEntity);
//     });
//     it('should return a PermissionDTO constructed by a PermissionEntity', () => {
//         const permission = new PermissionEntity(modelEntity, userEntity, roleEntity);
//         const permissionDTO = new PermissionDto(permission);
//         expect(permissionDTO).toBeTruthy();
//         expect(typeof permissionDTO).toBe('object');
//         expect(permissionDTO).toBeInstanceOf(PermissionDto);
//     });

// });
