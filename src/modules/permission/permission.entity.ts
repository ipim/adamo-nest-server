
import { AbstractEntity } from '../../common/abstract.entity';
import { PermissionDto } from './dto/PermissionDto';
// import { RoleType } from '../../constants/role-type';
// import { PasswordTransformer } from './password.transformer';

import {Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToOne, JoinColumn} from 'typeorm';
import {ApiProperty} from "@nestjs/swagger";
// import { RoleType } from 'constants/role-type';
import { RoleEntity } from '../role/role.entity';
import { UserEntity } from '../user/user.entity';

import { ModelEntity } from '../model/model.entity';
import { Exclude } from 'class-transformer';


@Entity({name: "permissions"})
export class PermissionEntity extends AbstractEntity<PermissionDto>{

    // @PrimaryGeneratedColumn("uuid")
    // public permissionID: string;

    
    @ApiProperty({ type: () => ModelEntity })
    // @Column({nullable: false, type: "uuid" })
    @OneToOne(type => ModelEntity)
    @JoinColumn()
    public model: ModelEntity;

    @ApiProperty()
    // @Column({nullable: false, type: "uuid" })
    @OneToOne(type => UserEntity)
    @JoinColumn()
    @ApiProperty({ type: () => UserEntity })
    public user: UserEntity;

   
    @ApiProperty({ type: () => RoleEntity })
    // @Column({nullable: false, type: "uuid" })
    @OneToOne(type => RoleEntity , {cascade: true})
    @JoinColumn()
    public role: RoleEntity;

    @Exclude()
    dtoClass = PermissionDto;

    constructor(model?: ModelEntity, user?: UserEntity, role?: RoleEntity) {
        super();
        this.model = model;
        this.user = user;
        this.role = role;
    }
}


