'use strict';

import { Get, Controller, UseGuards, UseInterceptors, Post, Body, Delete, Param, Put, Logger } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { RolesGuard } from '../../guards/role.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { PermissionEntity } from './permission.entity';
import { PermissionService } from './permission.service';
import { UpdateResult } from 'typeorm';

@Controller('permission')
@ApiTags('permission')
// @UseGuards(AuthenticatedGuard, RolesGuard)
// @UseInterceptors(AuthUserInterceptor)
// @ApiBearerAuth()
export class PermissionController {

    constructor(private readonly _permissionService: PermissionService) {
    }
    @Get()
    list() {
        return this._permissionService.list();
    }

    @Get('/:userID/:modelID')
    async getPermissionByUserAndModel(@Param('userID') userID: string, @Param('modelID') modelID: string) {
        return this._permissionService.getPermissionByUserAndModel(userID, modelID);

    }

    @Post('/create')
    create(@Body() entity: PermissionEntity) : Promise<PermissionEntity>{
        return this._permissionService.create(entity);
    }

    @Put('/update/:id')
    async update(@Param('id') id: string, @Body() updatPermissionEntity: any) {
        Logger.log('Body.', JSON.stringify(updatPermissionEntity))
        const updateResult = await this._permissionService.update(id, updatPermissionEntity);
        // Logger.log(updateResult.raw.affectedRows > 0);
        Logger.log(updateResult);
        return updateResult;
    }


    @Delete(':id')
    delete(@Param('id') id: string) {
        return this._permissionService.delete(id);
    }

    @Get(':id')
    getModel(id) {
        return this._permissionService.findPermissions(id);
    }

    // @Get('admin')
    // @Roles(RoleType.User)
    // @HttpCode(HttpStatus.OK)
    // async admin(@AuthUser() user: UserEntity) {
    //     return 'only for you admin: ' + user.firstName;
    // }

    // TODO create best PRACTICE for pagination https://github.com/bashleigh/nestjs-typeorm-paginate/blob/master/src/pagination.ts
}
