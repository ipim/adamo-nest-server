import {
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Post,
    UploadedFile,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
    ApiBearerAuth,
    ApiConsumes,
    ApiOkResponse,
    ApiTags,
} from '@nestjs/swagger';

import { AuthUser } from '../../decorators/auth-user.decorator';
import { ApiFile } from '../../decorators/swagger.schema';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { IFile } from '../../interfaces/IFile';
import { UserDto } from '../user/dto/UserDto';
import { UserEntity } from '../user/user.entity';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { LoginPayloadDto } from './dto/LoginPayloadDto';
import { UserLoginDto } from './dto/UserLoginDto';
import { UserRegisterDto } from './dto/UserRegisterDto';


/**
 * The auth controller provides no logout functionality because we use? token-based authentication.
 * Therefore we ensure deleting the token on client-side to be no longer able to login
 */
@Controller('auth')
@ApiTags('auth')
export class AuthController {
    constructor(
        public readonly userService: UserService,
        public readonly authService: AuthService,
    ) {}

    /**
 * @api                 {post} /authenticate authenticate
 * @apiDescription      Checks if post parameters email and password are set,
 *                      checks if email is in database,
 *                      checks if user already has a session,
 *                      checks if the password matches with the stored hash
 *                      and finally sets session.
 * @apiName             authenticate
 * @apiGroup            session
 * @apiParam            {String} email Mandatory email of a user
 * @apiParam            {String} password Mandatory password of a user
 * @apiSuccess          message success
 * @apiSuccessExample   Success-Response:
 *                      HTTP/1.1 200 OK
 *                      {message: 'success', success: true, data: data, email: 'maxmuster@gmail.com'}
 * @apiError            error Something went wrong
 * @apiErrorExample     Error-Response:
 *                      HTTP/1.1 400 Failure
 *                      'Something happend'
 *                      HTTP/1.1 401 Failure
 *                      'User and password do not match'
 *                      HTTP/1.1 404 Failure
 *                      'User not found in the database'
 * */
    @Post('login')
    // @UseGuards(LoginGuard)
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({
        type: LoginPayloadDto,
        description: 'User info with access token',
    })
    async userLogin(
        @Body() userLoginDto: UserLoginDto,
    ): Promise<LoginPayloadDto> {
        const userEntity = await this.authService.validateUser(userLoginDto);

        const token = await this.authService.createToken(userEntity);
        return new LoginPayloadDto(userEntity.toDto(), token);
    }

    @Post('register')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({ type: UserDto, description: 'Successfully Registered' })
    async userRegister(
        @Body() userRegisterDto: UserRegisterDto
    ): Promise<UserDto> {
        const createdUser = await this.userService.createUser(
            userRegisterDto
        );

        return createdUser.toDto();
    }

    /**
 * @api                 {get} /login_status login_status
 * @apiDescription      Checks the login status of a user
 * @apiName             login_status
 * @apiGroup            session
 * @apiSuccess          message Logged in as [userprofile]
 * @apiSuccessExample   Success-Response:
 *                      HTTP/1.1 200 OK
 *                      {message: 'logged in as [Admin]', email, profile, permission, success: true, loggedIn: true}
 * @apiError            message Not logged in
 * @apiErrorExample     Error-Response:
 *                      HTTP/1.1 200 Failure
 *                      {message: 'not logged in', success: true, loggedIn: false}
 */
    // @Get('me')
    // @HttpCode(HttpStatus.OK)

    // // @UseGuards(AuthGuard('jwt'))
    // @UseGuards(AuthGuard)
    // // @UseInterceptors(AuthUserInterceptor)
    // // @ApiBearerAuth()
    // @ApiOkResponse({ type: UserDto, description: 'current user info' })
    // getCurrentUser() {
    // // getCurrentUser(@AuthUser() user: UserEntity, @Req() request) {
    //     // Logger.log(`User: ${user}, Request: ${request}`);
    //     return user.toDto();
    // }
}
