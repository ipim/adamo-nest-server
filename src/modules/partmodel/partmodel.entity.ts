
import { AbstractEntity } from '../../common/abstract.entity';
import { PartModelDto } from './dto/PartModelDto';
import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {ApiProperty} from "@nestjs/swagger";
import { Exclude } from 'class-transformer';


@Entity({name: "models"})
export class PartModelEntity extends AbstractEntity<PartModelDto>{

    @PrimaryGeneratedColumn("uuid")
    public id: string;

    @ApiProperty()
    @Column({nullable: false})
    public modelName: string;

    // @ApiProperty()
    // @Column({nullable: false})
    // public timestampLastChange: number;

    @ApiProperty()
    @Column({nullable: false})
    public modelXML: string;

    @ApiProperty()
    @Column({nullable: false})
    public modelVersion: number;

    public toString(): string {
        return `${this.modelName}`;
    }

    @Exclude()
    dtoClass = PartModelDto;

}


