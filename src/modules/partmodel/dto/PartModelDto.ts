'use strict';

import { ApiPropertyOptional } from '@nestjs/swagger';

import { PartModelEntity } from '../partmodel.entity';
import { AbstractDto } from '../../../common/dto/AbstractDto';

export class PartModelDto extends AbstractDto {
    @ApiPropertyOptional()
    modelName: string;

    // @ApiPropertyOptional()
    // timestampLastChange: number;

    @ApiPropertyOptional()
    modelXML: string;

    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    public modelVersion: number;

    constructor(model: PartModelEntity) {
        super(model);
        this.modelName = model.modelName;
        this.id = model.id;
        this.modelVersion = model.modelVersion;
        // this.timestampLastChange= model.timestampLastChange;
        this.modelXML = model.modelXML;
    }
}
