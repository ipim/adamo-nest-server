'use strict';

import {
    Get,
    HttpCode,
    HttpStatus,
    Controller,
    Post,
    Body,
    Delete,
    Param,
    Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Modelling_RulesService } from './modelling_rules.service';

@Controller('modelling_rules')
@ApiTags('modelling_rules')
@ApiBearerAuth()
export class Modelling_RulesController {
    constructor(private readonly modelling_RulesService: Modelling_RulesService) {}
}