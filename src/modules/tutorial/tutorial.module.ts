import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Modelling_QuestionModule } from './modelling_question/modelling_question.module';
import { CategoryModule } from './category/category.module';
import { Tg_ModellingModule } from './tg_modelling/tg_modelling.module';
import { Tg_MultiplechoiceModule } from './tg_multiplechoice/tg_multiplechoice.module';
import { Tg_Multiplechoice_AnsweredModule } from './tg_multiplechoice_answered/tg_multiplechoice_answered.module';
import { TestModule } from './test/test.module';
import { Tg_IntroModule } from './tg_intro/tg_intro.module';
import { Multiplechoice_Question_AnswerModule } from './multiplechoice_question_answer/multiplechoice_question_answer.module';
import { Multiplechoice_QuestionModule } from './multiplechoice_question/multiplechoice_question.module';
import { IntroModule } from './intro/intro.module';

@Module({
    imports: [
        IntroModule,
        Modelling_QuestionModule,
        Multiplechoice_Question_AnswerModule,
        Multiplechoice_QuestionModule,
        Modelling_QuestionModule,
        CategoryModule,
        Tg_ModellingModule,
        Tg_MultiplechoiceModule,
        Tg_Multiplechoice_AnsweredModule,
        TestModule,
        Tg_IntroModule,
    ],
    controllers: [],
    exports: [],
    providers: [
        
    ],
})
export class TutorialModule {}
