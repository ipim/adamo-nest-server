'use strict';

import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../../../../common/dto/AbstractDto';
import { IntroEntity } from '../intro.entity';

export class IntroDto extends AbstractDto {
    @ApiPropertyOptional()
    public intro_id: string;

    @ApiPropertyOptional()
    public intro_text: string;

    @ApiPropertyOptional()
    public intro_text_de: string;

    @ApiPropertyOptional()
    public intro_categories: string;

    @ApiPropertyOptional()
    public intro_identifier: number;

    @ApiPropertyOptional()
    public intro_intro_svg_intro_page: string;

    constructor(intro: IntroEntity) {
        super(intro);
        this.intro_id = intro.intro_id;
        this.intro_text = intro.intro_text;
        this.intro_text_de = intro.intro_text_de;
        this.intro_categories = intro.intro_categories;
        this.intro_identifier = intro.intro_identifier;
    }
}
