import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import {
    Column,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

import { AbstractEntity } from '../../../common/abstract.entity';
import { CategoryEntity } from '../category/category.entity';
import { Tg_IntroEntity } from '../tg_intro/tg_intro.entity';
import { IntroDto } from './dto/IntroDto';

@Entity({ name: 'intro' })
export class IntroEntity extends AbstractEntity<IntroDto> {
    @PrimaryGeneratedColumn('uuid')
    @ManyToOne(
        (type) => Tg_IntroEntity,
        (category) => category.tg_intro_intro_id,
    )
    public intro_id: string;

    @ApiProperty()
    @Column()
    public intro_text: string;

    @ApiProperty()
    @Column()
    public intro_text_de: string;

    @ApiProperty()
    @Column()
    @OneToMany((type) => CategoryEntity, (category) => category.category_id)
    public intro_categories: string;

    @ApiProperty()
    @Column()
    public intro_identifier: number;

    @Exclude()
    dtoClass = IntroDto;
}
