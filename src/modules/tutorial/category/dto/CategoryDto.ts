import { CategoryEntity } from '../category.entity';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { AbstractDto } from '../../../../common/dto/AbstractDto';

export class CategoryDto extends AbstractDto {
    @ApiPropertyOptional()
    public category_id: string;

    @ApiPropertyOptional()
    public category_name: string;

    @ApiPropertyOptional()
    public category_identifier: number;

    constructor(category: CategoryEntity) {
        super(category);
        this.category_id = category.category_id;
        this.category_name = category.category_name;
        this.category_identifier = category.category_identifier;
    }
}
