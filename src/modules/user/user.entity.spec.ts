import { UserEntity } from './user.entity';
import { RoleType } from './../../constants/role-type';
import { UserDto } from './dto/UserDto';


describe('UserEntity class', () => {
  it('should make a user', () => {
      const userJSONFixture = {
          firstName: 'FirstName',
          lastName: 'LastName',
          username: 'UserName',
          role: RoleType.User,
          email: 'EMailAdresse',
          password: '1234',
      }
    const user = new UserEntity(userJSONFixture.firstName, userJSONFixture.lastName, userJSONFixture.username, userJSONFixture.role, userJSONFixture.email, userJSONFixture.password);
    expect(user).toBeTruthy();
    expect(user.firstName).toBe(userJSONFixture.firstName);
    expect(user.lastName).toBe(userJSONFixture.lastName);
    expect(user.username).toBe(userJSONFixture.username);
    expect(user.role).toBe(userJSONFixture.role);
    expect(user.email).toBe(userJSONFixture.email);
    expect(user.password).toBe(userJSONFixture.password);
  });
  
  it('should return a UserDTO constructed by a UserEntity', () => {
    const user = new UserEntity('FirstName', 'LastName', 'UserName', RoleType.User, 'EMailAdresse', '1234');
    const UserDTO = new UserDto(user);
    expect(UserDTO).toBeTruthy();
    expect(typeof UserDTO).toBe("object");
    expect(UserDTO).toBeInstanceOf(UserDto);
  });
});