import { Entity, Column, OneToMany } from 'typeorm';

import { AbstractEntity } from '../../common/abstract.entity';
import { RoleType } from '../../common/constants/role-type';
import { UserDto } from './dto/UserDto';
import { PermissionEntity } from '../permission/permission.entity';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity({ name: 'users' })
export class UserEntity extends AbstractEntity<UserDto> {
    @Column({ nullable: true })
    firstName: string;

    @Column({ nullable: true })
    lastName: string;

    @Column({ type: 'enum', enum: RoleType, default: RoleType.USER })
    role: RoleType;

    @Column({ unique: true, nullable: true })
    email: string;

    @Column({ nullable: true })
    password: string;

    // @Column({ nullable: true })
    // phone: string;
    @OneToMany(type => PermissionEntity, permission => permission.id)
    @ApiPropertyOptional({ type: () => PermissionEntity })
    permission: PermissionEntity[];

    // @Column({ nullable: true })
    // avatar: string;
    @Exclude()
    dtoClass = UserDto;

    constructor(firstName: string, lastName: string, username: string, role: RoleType, email: string, password: string) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        // this.username = username;
        this.role = role;
        this.email = email;
        this.password = password;
    }
}
