'use strict';

import {
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Query,
    UseGuards,
    UseInterceptors,
    ValidationPipe,
    Post,
    Body,
    Logger,
    Put,
    Param,
    Delete,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

import { RoleType } from '../../common/constants/role-type';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { Roles } from '../../decorators/roles.decorator';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
//import { UsersPageDto } from './dto/UsersPageDto';
//import { UsersPageOptionsDto } from './dto/UsersPageOptionsDto';
import { UserEntity } from './user.entity';
import { UserService } from './user.service';
// import { PasswordTransformer } from './password.transformer';
import { getManager } from 'typeorm';
import { JwtAuthGuard } from '../../guards/jwt-auth.guard';
import { UserDto } from './dto/UserDto';
import { ModelEntity } from '../model/model.entity';
import { ModelService } from '../model/model.service';
import { PermissionEntity } from '../permission/permission.entity';
import { PermissionService } from '../permission/permission.service';
import { RoleEntity } from '../role/role.entity';

@Controller('user')
@ApiTags('user')
// @UseGuards(AuthGuard, RolesGuard)
// @UseInterceptors(AuthUserInterceptor)
// @UseGuards(AuthGuard, RolesGuard)
// @ApiBearerAuth()
export class UserController {

  constructor(private readonly userService: UserService,
    private modelService: ModelService,
    private permissionService: PermissionService) { }



  @Get()
  // @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.OK)
  async list() : Promise<UserEntity[]> {
    return await this.userService.list();
  }

    // @Get('users')
    // @Roles(RoleType.ADMIN)
    // @HttpCode(HttpStatus.OK)
    // @ApiResponse({
    //     status: HttpStatus.OK,
    //     description: 'Get users list',
    //     type: UsersPageDto,
    // })
    // getUsers(
    //     @Query(new ValidationPipe({ transform: true }))
    //     pageOptionsDto: UsersPageOptionsDto,
    // ): Promise<UsersPageDto> {
    //     return this.userService.getUsers(pageOptionsDto);
    // }
  // @Get()
  // @HttpCode(HttpStatus.OK)
  // async


  @Post('create')
  async create(@Body() userData: any): Promise<any> {
    Logger.log(`user create ${userData}`)

    const createdUser = await this.userService.create(userData);
    const allModels = await getManager().find(ModelEntity);
    const readWriteRole = await getManager().findOne(RoleEntity, { roleName: 'readWrite' })
    allModels.forEach(model => {
      this.permissionService.create(
        getManager().create(PermissionEntity, {
          user: createdUser,
          role: readWriteRole,
          model: model,

        })
      )
    });
    // also generate permission
    const selectDistinctModelsFromModel = "";
    const insertIntoPermissionMidUidRid = "";
    const values = ["row.mid", createdUser.id, RoleType.USER]
    return userData;


    /*
     db.query('' +
                  'SELECT DISTINCT(mid) as mid ' +
                  'FROM model')
                .then(function (rows) {
                  rows.forEach(function (row) {
                    db.oneOrNone('' +
                      'INSERT INTO permission (mid, uid, rid)\n' +
                      'SELECT $1, $2, $3', [row.mid, _uid, 6, ])
                  });
                })
    */
  }

  /**
   * We use this also for password?
   *
   * @param id
   * @param userData
   */
  @Put(':id/update')
  async update(@Param('id') id, @Body() userData: UserEntity): Promise<any> {
    // userData.id = Number(id);
    userData.id = id;
    console.log('Update #' + userData.id)
    return this.userService.update(userData);
  }

//   @Post()
//   @HttpCode(HttpStatus.OK)
//   async changePassword(@Body() userData: any) {
//     const oldUser = await this.userService.findUser({ id: userData.id })
//     const passwordTransformer = new PasswordTransformer()
//     oldUser.password = passwordTransformer.to(userData.password)

//     return this.userService.update(oldUser)
//   }


  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    // also remove the permission .. maybe typeorm is good enough to remove it also
    const user = await this.userService.findUsers(id);
    this.permissionService.deleteByUser(user[0])
    return this.userService.delete(id);

    const deletedUser = this.userService.delete(id);
    deletedUser.then(data => data.raw)
    //  db.oneOrNone('delete from permission where uid = $1; delete from users where uid = $1', [uid])

  }


  // TODO create best PRACTICE for pagination https://github.com/bashleigh/nestjs-typeorm-paginate/blob/master/src/pagination.ts
}
