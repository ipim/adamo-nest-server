// import * as request from 'supertest';
// import { Test } from '@nestjs/testing';
// import { INestApplication } from '@nestjs/common';
// import { RoleModule } from './role.module';
// import { RoleService } from './role.service';
// import { UserModule } from 'modules/user/user.module';
// import { UserRepository } from 'modules/user/user.repository';
// import { AuthModule } from 'modules/auth/auth.module';

// describe('Roles', () => {
//   let app: INestApplication;
//   let roleService = { find: () => ['test'] };

//   beforeAll(async () => {
//     const moduleRef = await Test.createTestingModule({
//       imports: [AuthModule, RoleModule],
//     })
//       .overrideProvider(RoleService)
//       .useValue(roleService)
//       .compile();

//     app = moduleRef.createNestApplication();
//     await app.init();
//   });

//   it(`/GET role`, () => {
//     return request(app.getHttpServer())
//       .get('/')
//       .expect(200)
//       .expect({
//         data: roleService.find(),
//       });
//   });

//   afterAll(async () => {
//     await app.close();
//   });
// });