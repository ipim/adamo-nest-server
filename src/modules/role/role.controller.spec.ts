import { Test, TestingModule } from '@nestjs/testing';
import { RoleController } from './role.controller';
import { RoleService } from './role.service';
import { RoleEntity } from './role.entity';
import { plainToClass } from 'class-transformer';

const testCat3 = 'Test Role 3';

describe('Role Controller', () => {
  let controller: RoleController;
  const roleFixture = {
        id: '1',
        roleName: 'TestRole1',
        canWrite: false,
        canRead: false,
        isAdmin: false,
    };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleController],
      providers: [
        {
          provide: RoleService,
          useValue: {
              find: jest
              .fn()
              .mockReturnValue([
                new RoleEntity('1', 'admin', true, true, true),
                new RoleEntity('2', 'Test Role 2'),
              ]),
              create: jest
              .fn()
              .mockReturnValue(plainToClass(RoleEntity, roleFixture)),
              update: jest
              .fn()
              .mockReturnValue(new RoleEntity('3', testCat3)),
              deleteCat: jest.fn().mockReturnValue(true),
          },
        },
      ],
    }).compile();

    controller = module.get<RoleController>(RoleController);
  });

  /**
   * These all may seem like simple tests that don't do much, but in reality
   * the controller itself is pretty simple. Call a service and return it's value,
   * the complicated stuff comes in either in the service, a pipe, or the interceptor
   */
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('list all Roles - listRoles() ',  () => {
    it('should get the list of cats', async () => {
      const roles = await controller.listRoles();
      expect(typeof roles).toBe('object');
      expect(roles[0].id).toBe('1');
      expect(roles[1].roleName).toBe('Test Role 2');
      expect(roles.length).toBe(2);
    });
  });
  describe('create Role - create', () => {

        it('should return a new role', async () => {
            const role = await controller.create(plainToClass(RoleEntity, roleFixture));
            expect(role.id).toBe('1');
            expect(role.roleName).toBe('TestRole1');
        });
    });
  describe('update Role - update()', () => {
    // it('should get the role matching the id', () => {
    //   const retCat = controller.(1);
    //   expect(typeof retCat).toBe('object');
    //   expect(retCat.id).toBe(1);
    //   expect(retCat.name).toBe(testCat1);
    // });
  });
    // describe('deleteCat', () => {
  //   it('should return true that there was a deletion', () => {
  //     const delReturn = controller.deleteCat(2);
  //     expect(typeof delReturn).toBe('boolean');
  //     expect(delReturn);
  //   });
  // });
});
