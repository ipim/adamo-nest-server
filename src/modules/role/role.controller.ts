'use strict';

import {
    Get,
    HttpCode,
    HttpStatus,
    Controller,
    UseGuards,
    UseInterceptors,
    Post,
    Body,
    Delete,
    Param,
    Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { Roles } from '../../decorators/roles.decorator';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { RolesGuard } from '../../guards/role.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
// import { UserEntity } from '../user/user.entity';
import { RoleService } from './role.service';
import { RoleEntity } from './role.entity';
import { RoleDto } from './dto/RoleDto';

@Controller('role')
@ApiTags('role')
// @UseGuards(AuthenticatedGuard, RolesGuard)
// @UseInterceptors(AuthUserInterceptor)
// @ApiBearerAuth()
export class RoleController {
    constructor(private readonly _roleService: RoleService) {}
    /**
     *
     */
    @Get()
    async listRoles() {
        return this._roleService.getAll();
    }

     /**
     * We use this also for password
     * @param id
     * @param userData
     */
    @Put(':id/update')
    async update(@Param('id') id, @Body() roleData: RoleEntity): Promise<any> {
        // userData.id = Number(id);
        // Set the roleData.id because it is missing in Data from Client?!
        roleData.id = id;
        console.log('Update #' + roleData.id)
        return this._roleService.updateOne(roleData);
    }

    /**
     *
     * @param entity
     */
    @Post('/create')
    create(@Body() entity: Partial<RoleDto>) : Promise<any> {
        return this._roleService.createOne(entity);
    }

    @Delete(':id/delete')
    async delete(@Param('id') id): Promise<any> {
      return this._roleService.deleteOne(id);
      // Maybe deleting all role entries in Permissions with this role and set them back to Default ist an valid option? TODO
    }

}
