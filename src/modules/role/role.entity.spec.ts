import { RoleEntity } from './role.entity';
import { RoleDto } from './dto/RoleDto';

describe('RoleEntity class', () => {
  it('should make a role with no fields', () => {
    const role = new RoleEntity('1', 'user');
    expect(role).toBeTruthy();
    expect(role.id).toBe('1');
    expect(role.roleName).toBe('user');
    expect(role.canRead).toBe(false);
    expect(role.canWrite).toBe(false);
    expect(role.isAdmin).toBe(false);
  });
  it('should make a role with admin ', () => {
    const role = new RoleEntity('2', 'admin', true, true, true);
    expect(role).toBeTruthy();
    expect(role.id).toBe('2');
    expect(role.roleName).toBe('admin');
    expect(role.canRead).toBe(true);
    expect(role.canWrite).toBe(true);
    expect(role.isAdmin).toBe(true);
  });
  it('should make a roledemo user', () => {
    const role = new RoleEntity('3', 'demoUser', true, true, false);
    expect(role).toBeTruthy();
    expect(role.id).toBe('3');
    expect(role.roleName).toBe('demoUser');
    expect(role.canRead).toBe(true);
    expect(role.canWrite).toBe(true);
    expect(role.isAdmin).toBe(false);
  });
  it('should return a string representation of a role', () => {
    const role = new RoleEntity('3', 'demoUser', true, true, false);
    expect(role).toBeTruthy();
    expect(role.id).toBe('3');
    expect(role.roleName).toBe('demoUser');
    expect(typeof role.toString()).toBe('string');
    expect(role.toString()).toBe('demoUser')
  });

  it('should return a RoleDTO constructed by a RoleEntity', () => {
    const role = new RoleEntity('3', 'demoUser', true, true, false);
    const roleDTO = new RoleDto(role);
    expect(roleDTO).toBeTruthy();
    expect(typeof roleDTO).toBe("object")
    expect(roleDTO).toBeInstanceOf(RoleDto)
  });
});