import { AbstractEntity } from '../../common/abstract.entity';
import { RoleDto } from './dto/RoleDto';
// import { RoleType } from '../../constants/role-type';
// import { PasswordTransformer } from './password.transformer';

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity({ name: 'role' })
export class RoleEntity extends AbstractEntity<RoleDto> {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ApiProperty()
    @Column({ nullable: false})
    public roleName: string;

    @ApiProperty()
    @Column({ nullable: false, default: false })
    public canRead: boolean;

    @ApiProperty()
    @Column({ nullable: false, default: false })
    public canWrite: boolean;

    @ApiProperty()
    @Column({ nullable: false, default: false })
    public isAdmin: boolean;

    @Exclude()
    public toString(): string {
        return `${this.roleName}`;
    }

    @Exclude()
    dtoClass = RoleDto;

    constructor(id: string, roleName: string, canRead?: boolean, canWrite?: boolean, isAdmin?: boolean) {
        super();
        this.id = id;
        this.roleName = roleName;
        this.canRead = canRead || false;
        this.canWrite = canWrite || false;
        this.isAdmin = isAdmin || false;
    }
}
