import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RoleEntity } from './role.entity';
import { RoleService } from './role.service';
import { plainToClass } from 'class-transformer';
import { RoleRepository } from './role.repository';
import { RoleDto } from './dto/RoleDto';

const testRole1 = '1';
const testName1 = 'Test Role 1';

const roleArray = [
    new RoleEntity(testRole1, testName1),
    new RoleEntity('2', 'Test Role 2', true, true, true),
    new RoleEntity('3', 'Test Role 3', false, false, false),
];

const oneRole = new RoleEntity(testRole1, testName1, false, false, false);

describe('RoleService', () => {
    let service: RoleService;
    let repo: RoleRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [

                RoleService,
                {
                    provide: RoleRepository, //getRepositoryToken(RoleEntity),
                    // define all the methods that you use from the roleRepo
                    // give proper return values as expected or mock implementations, your choice
                    useValue: {
                        find: jest.fn().mockResolvedValue(roleArray),
                        findOneOrFail: jest.fn().mockResolvedValue(oneRole),
                        create: jest.fn().mockReturnValue(oneRole),
                        insertOne: jest.fn().mockReturnValue(oneRole),
                        save: jest.fn(),
                        // as these do not actually use their return values in our sample
                        // we just make sure that their resolee is true to not crash
                        update: jest.fn().mockResolvedValue(true),
                        // as these do not actually use their return values in our sample
                        // we just make sure that their resolee is true to not crash
                        delete: jest.fn().mockResolvedValue(true),
                    },
                },
            ],
        }).compile();

        service = module.get<RoleService>(RoleService);
        repo = module.get<Repository<RoleEntity>>(RoleRepository); //getRepositoryToken(RoleEntity));
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
    describe('getAll', () => {
        it('should return an array of roles', async () => {
            const roles = await service.getAll();
            expect(roles).toEqual(roleArray);
        });
    });
    describe('getOne', () => {
        it('should get a single role', () => {
            const repoSpy = jest.spyOn(repo, 'findOneOrFail');
            expect(service.getOne('1')).resolves.toEqual(oneRole);
            expect(repoSpy).toBeCalledWith({ id: '1' });
        });
    });
    describe('getOneByName', () => {
        it('should get one role', () => {
            const repoSpy = jest.spyOn(repo, 'findOneOrFail');
            expect(service.getOneByName(testRole1)).resolves.toEqual(oneRole);
            expect(repoSpy).toBeCalledWith({ roleName: testRole1 });
        });
    });
    describe('insertOne', () => {
        it('should successfully insert a role', () => {
            const testRole = plainToClass(RoleEntity, {
                id: '1',
                roleName: testName1,
            });
            expect(service.createOne(plainToClass(RoleEntity, testRole))).resolves.toEqual(testRole);
            expect(repo.create).toBeCalledTimes(1);
            expect(repo.create).toBeCalledWith(testRole);
            expect(repo.save).toBeCalledTimes(1);
        });
    });
    // describe('updateOne', () => {
    //     it('should call the update method', async () => {
    //         const role = await service.updateOne({
    //             id: '1',
    //             roleName: testRole1,
    //         });
    //         expect(role).toEqual(oneRole);
    //         expect(repo.update).toBeCalledTimes(1);
    //         expect(repo.update).toBeCalledWith(
    //             { id: 'a uuid' },
    //             { name: testRole1, breed: testBreed1, age: 4, id: 'a uuid' },
    //         );
    //     });
    // });
    // describe('deleteOne', () => {
    //     it('should return {deleted: true}', () => {
    //         expect(service.deleteOne('a uuid')).resolves.toEqual({ deleted: true });
    //     });
    //     it('should return {deleted: false, message: err.message}', () => {
    //         const repoSpy = jest
    //             .spyOn(repo, 'delete')
    //             .mockRejectedValueOnce(new Error('Bad Delete Method.'));
    //         expect(service.deleteOne('a bad uuid')).resolves.toEqual({
    //             deleted: false,
    //             message: 'Bad Delete Method.',
    //         });
    //         expect(repoSpy).toBeCalledWith({ id: 'a bad uuid' });
    //         expect(repoSpy).toBeCalledTimes(1);
    //     });
    // });

    // a bit of an in depth test to make sure the cats are staying in memory
    describe('getAll, addRole, getAll, deleteRole, getAll, findRole', () => {
        /**
         * Due to how javascript assign objects values by reference and not by value
         * it is imperative to run each test right after the function runs.
         * Also a good idea to get the length immediately as that will not change but the array will
         */
        it('should get all cats, create new cat, get all cats again, delete a cat, get all cats and find a specified cat', async () => {
            const firstRoleSet = await service.getAll();
            const firstRoleSetLength = firstRoleSet.length;
            expect(firstRoleSet.length).toBe(3);
            // const newRole = await service.createOne( newRoleDto(roleName: 'admin', canRead: true, canWrite: true, isAdmin: true));
            // expect(newRole).toEqual({roleName: 'admin', canRead: true, canWrite: true, isAdmin: true});
            // const secondRoleSet = await service.getAll();
            // expect(secondRoleSet.length).toBe(firstRoleSetLength + 1);
            // expect(secondRoleSet).toContainEqual(newRole);
            const deleted = await service.deleteOne('2');
            expect(deleted);
            // const thirdRoleSet = service.getAll();
            // expect(thirdRoleSet.length).toBe(firstRoleSetLength);
            // const foundRole = service.getById(3);
            // expect(foundRole).toEqual({
            //     id: 3,
            //     name: 'Aqua',
            //     breed: 'Maine Coon',
            //     age: 5,
            // });
        });
    });
});
