
import { RoleEntity } from '../../role/role.entity';
import { PermissionEntity } from '../../permission/permission.entity';
import { Connection, ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
    expression: (connection: Connection) =>

    connection.createQueryBuilder()
        .select(['model.id as id', 'model.model_name as modelName', "model.created_at", "model.updated_at"])

        .leftJoin(PermissionEntity, "permission", "model.id = permission.model_id")
        .leftJoin(RoleEntity, "role", "role.id = permission.role_id")
        .select(["role.can_read", "role.can_write", "role.is_admin"])

        // .where("permission.user_id= :id", {id: userid} )

        .orderBy("model.model_name", "ASC")

        .orderBy("model.model_version", "DESC")
})
export class ModelByUserPermission  {
    @ViewColumn()
    modelName: string;

    @ViewColumn()
    modelXML: string;

    @ViewColumn()
    id: string;

    @ViewColumn()
    public modelVersion: number;

    @ViewColumn()
    public canRead: boolean;

    @ViewColumn()
    public canWrite: boolean;

    }
