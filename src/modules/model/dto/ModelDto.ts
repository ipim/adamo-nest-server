
import { ApiPropertyOptional } from '@nestjs/swagger';

import { ModelEntity } from '../model.entity';
import { AbstractDto } from '../../../common/dto/AbstractDto';
import { IsString, IsOptional, IsNumber } from 'class-validator';

export class ModelDto extends AbstractDto {
    @ApiPropertyOptional()
    @IsString() 
    @IsOptional()
    modelName: string;

    @ApiPropertyOptional()
    @IsString() 
    @IsOptional()
    modelXML: string;

    @ApiPropertyOptional()
    @IsString() 
    @IsOptional()
    id: string;

    @ApiPropertyOptional()
    @IsNumber() 
    @IsOptional()
    public modelVersion: number = 1;

    constructor(model: ModelEntity) {
        super(model);
        this.modelName = model.modelName;
        this.id = model.id;
        this.modelVersion = model.modelVersion || 1;
        this.modelXML = model.modelXML;
    }
}
