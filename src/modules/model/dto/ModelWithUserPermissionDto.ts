
import { ApiPropertyOptional } from '@nestjs/swagger';

import { ModelEntity } from '../model.entity';
import { AbstractDto } from '../../../common/dto/AbstractDto';
import { RoleEntity } from '../../role/role.entity';

export class ModelWithUserPermissionDto extends AbstractDto {
    @ApiPropertyOptional()
    modelName: string;

    @ApiPropertyOptional()
    modelXML: string;

    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    public modelVersion: number;

    @ApiPropertyOptional()
    public canRead: boolean;

    @ApiPropertyOptional()
    public canWrite: boolean;

    // constructor(model: ModelEntity, role: RoleEntity) {
    //     super(model);
    //     this.modelName = model.modelName;
    //     this.id = model.id;
    //     this.modelVersion = model.modelVersion;
    //     this.modelXML = model.modelXML;
    //     this.canRead = role.canRead;
    //     this.canWrite = role.canWrite;
    //
    // }
//

}
