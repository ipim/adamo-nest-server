'use strict';

import {
    Get,
    Controller,
    UseGuards,
    UseInterceptors,
    Logger,
    Post,
    Param,
    Body,
} from '@nestjs/common';
import {
    ApiBearerAuth,
    ApiTags,
    ApiResponse,
    ApiOperation,
} from '@nestjs/swagger';

import { RolesGuard } from '../../guards/role.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { ModelService } from './model.service';
import { PermissionService } from '../permission/permission.service';
import { UserService } from '../user/user.service';
import { ModelDto } from './dto/ModelDto';
import { RoleService } from '../role/role.service';
import { RoleEntity } from '../role/role.entity';
import { FindOneOptions } from 'typeorm';
import { WrapperResponse } from '../../common/dto/WrapperResponse';
import { ResponseState } from '../../common/enum/state';
import { ModelEntity } from './model.entity';
@Controller('model')
@ApiTags('model')
// @UseGuards(AuthGuard) // der hier breaked..
// @UseGuards(AuthGuard, RolesGuard)
@UseGuards(RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
// accessToken:
// "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjhhNThkODQ1LTRiOWEtNDhjOC1hYWNjLWY2NjA5NThjZTVlZSIsImlhdCI6MTU2Mzc4MTk0MX0.N95OcCZld476xNJS1ke9lxFgy3iQMt0feW-zgTn7OfI"
export class ModelController {
    constructor(
        private readonly _modelService: ModelService,
        private readonly permissionService: PermissionService,
        private readonly userService: UserService,
        private readonly _roleService: RoleService,
    ) {}

    @Get('all')
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({
        status: 401,
        description: 'Unauthorized. Mostly because user is not logged in',
    })
    async listModels() {
        return await this._modelService.list();
    }

    @Get('all/:id')
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiResponse({
        status: 401,
        description: 'Unauthorized. Mostly because user is not logged in',
    })
    async listModelsByUser(@Param('id') id: string) {
        return await this._modelService.findModelbyUserPermission(id);
    }

    // @Get()
    // async getModel(@))

    @Get('changes')
    async changedModels() {
        return await this._modelService.getChangedModels();
    }

    @Post('create')
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: ModelDto,
    })
    // @ApiCreatedResponse({
    //     description: 'The record has been successfully created.',
    // })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async create(@Body() entity: Partial<ModelDto>): Promise<any> {
        // console.log(JSON.stringify(entity)
        // Logger.log('create entity' + JSON.stringify(entity));
        // create permissions for entity
        // this.permissionService.createPermission()

        //  const users: UserEntity[] = await queryRunner.manager.getRepository(UserEntity)
        //     .createQueryBuilder('users')
        //     .getMany();
        // const readWriteRole: RoleEntity = await queryRunner.manager.getRepository(RoleEntity)
        //     .createQueryBuilder('role')
        //     .where('role.roleName = :roleName', { roleName: 'readWrite' })
        //     .getOne();

        // // const array = this.buildPermission(users, model, readWriteRole);
        // // Logger.log(array);
        // //     canWriteRole.s

        // for (const user of users) {
        //     const permission =  await queryRunner.manager.create(PermissionEntity, {
        //         user,
        //         role: readWriteRole,
        //         model: savedModel,

        //     });
        //     let savedPermission: PermissionEntity = null;

        //     await queryRunner.manager.save(permission).then((permission) => savedPermission = <PermissionEntity>permission);

        //     console.log(savedPermission);
        // }
        return await this._modelService.create(entity).then(async (data) => {
            Logger.log('data:', data.toString());
            const allUsers = await this.userService.list();

            const currentModel = await this._modelService.findOneOrFail(
                data.id,
            );
            const readWriteRole = await this._roleService.getOneByName( 'readWrite');
            // const readWriteRole: RoleEntity = await queryRunner.manager.getRepository(RoleEntity)
            // .createQueryBuilder('role')
            // .where('role.roleName = :roleName', { roleName: 'readWrite' })
            // .getOne();

            Logger.log(currentModel);
            for (let user of allUsers) {
                Logger.log(user);
                await this.permissionService.createPermissionForNewModel(
                    currentModel,
                    user,
                    readWriteRole,
                );
            }
            return data;
        });
    }
    @Post('upsert')
    // @ApiOperation({ title: 'Update existing Model' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully updated.',
        type: ModelDto,
    })
    // @ApiCreatedResponse({
    //     description: 'The record has been successfully created.',
    // })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async upsert(@Body() entity: ModelEntity): Promise<WrapperResponse> {
        console.log(JSON.stringify(entity))
        Logger.log('upsert entity' + JSON.stringify(entity));
        // create permissions for entity
        // this.permissionService.createPermission()

        const currentModel = await  this._modelService.findOneOrFail(entity.id)
        Logger.log(currentModel.modelXML === entity.modelXML)
        if(currentModel.modelXML === entity.modelXML) {
            return new WrapperResponse(ResponseState.NO_CHANGES, entity)
        } else {
            return new WrapperResponse(ResponseState.OK, await this._modelService.update(entity))
        }
        // .then(async data => {
        // const allUsers = await this.userService.list();
        // const currentModel = await this._modelService.findOneOrFail(data.id);
        // for (let user of allUsers) {
        //     await this.permissionService.createPermissionForNewModel(
        //         currentModel,
        //         user,
        //     );
        // }
        // return data;
        // });
    }
    // @Delete(":id")
    // delete(@Param("id") id: string) {
    //     this.modelService.delete(id)
    // }

    @Get(':id/:version')
    @ApiResponse({
        status: 200,
        description: 'The found model for given id and version',
        type: ModelDto,
    })
    // @ApiQuery({ name: 'role', enum: ['Admin', 'Moderator', 'User'] })
    async getModel(@Param('id') id: string, @Param('version') version: string) {
        return await this._modelService.findModelByIdAndVersion(id, +version);
    }

    // @Get(':id/')
    // @ApiResponse({
    //     status: 200,
    //     description: 'The found model for given id and the newest version',
    //     type: ModelDto,
    //   })
    // // @ApiQuery({ name: 'role', enum: ['Admin', 'Moderator', 'User'] })
    // async getModelbyID(@Param('id') id: string, @Param('version') version: string) {
    //     // we need seperate type for version!
    //     return await this.modelService.findModelById(id);
    // }

    // @Get('admin')
    // @Roles(RoleType.User)
    // @HttpCode(HttpStatus.OK)
    // async admin(@AuthUser() model: ModelEntity) {
    //     return 'only for you admin: ' + model;
    // }

    // TODO create best PRACTICE for pagination https://github.com/bashleigh/nestjs-typeorm-paginate/blob/master/src/pagination.ts
}
