// import { ModelEntity } from './model.entity'
// import { ModelDto } from './dto/ModelDto';

// describe('ModelEntity class', () => {
//     it('should make a model', () => {
//         const model = new ModelEntity('1', 'model', 'modelxml', 1);
//         expect(model).toBeTruthy();
//         expect(model.id).toBe('1');
//         expect(model.modelName).toBe('model');
//         expect(model.modelXML).toBe('modelxml');
//         expect(model.modelVersion).toBe(1);
//     });
//     it('should return a string representation of a model', () => {
//         const model = new ModelEntity('1', 'model', 'modelxml', 1);
//         expect(model).toBeTruthy();
//         expect(model.id).toBe('1');
//         expect(model.modelName).toBe('model');
//         expect(typeof model.toString()).toBe('string');
//         expect(model.toString()).toBe('model');
//     });
//     it('should return a ModelDTO constructed by a ModelEntity', () => {
//         const model = new ModelEntity('1', 'model', 'modelxml', 1);
//         const modelDTO = new ModelDto(model);
//         expect(modelDTO).toBeTruthy();
//         expect(typeof modelDTO).toBe("object")
//         expect(modelDTO).toBeInstanceOf(ModelDto)
//     });
    
// })