
import { AbstractEntity } from '../../common/abstract.entity';
import { ModelDto } from './dto/ModelDto';
import {Column, Entity, PrimaryGeneratedColumn, Index} from 'typeorm';
import {ApiProperty} from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity({name: 'models'})
@Index(["id", "modelVersion"], { unique: true })
export class ModelEntity extends AbstractEntity<ModelDto> {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ApiProperty()
    @Column({unique: true, nullable: false})
    public modelName: string;

    @ApiProperty()
    @Column({nullable: false})
    public modelXML: string;

    @ApiProperty()
    @Column({nullable: false, default: 1})
    public modelVersion: number;

    @Exclude()
    public toString(): string {
        return `${this.modelName}`;
    }

    @Exclude()
    dtoClass = ModelDto;
    // constructor()
    // constructor(id?: string, modelName?: string, modelXML?: string, modelVersion?: number) 
    // {
        // super();
        // this.id = id;
        // this.modelName = modelName|| null;
        // this.modelXML = modelXML|| null;
        // this.modelVersion = modelVersion|| null;
    // }

}

