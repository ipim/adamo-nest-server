import './boilerplate.polyfill';

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './shared/services/config.service';
import { AuthModule } from './modules/auth/auth.module';
import { PermissionModule } from './modules/permission/permission.module';
import { ModelModule } from './modules/model/model.module';
import { UserModule } from './modules/user/user.module';
import { CategoryModule } from './modules/tutorial/category/category.module';
import { contextMiddleware } from './middlewares';
import { SharedModule } from './shared/shared.module';
import { RoleModule } from './modules/role/role.module';
import { PassportModule } from '@nestjs/passport';
import { TutorialModule } from './modules/tutorial/tutorial.module';

@Module({
    imports: [
        AuthModule,
        CategoryModule,
        // MathModule,
        ModelModule,

        PassportModule.register({
            defaultStrategy: 'local',
            session: true,
        }),
        PermissionModule,
        RoleModule,
        TutorialModule,
        TypeOrmModule.forRootAsync({
            imports: [SharedModule],
            useFactory: (configService: ConfigService) =>
                configService.typeOrmConfig,
            inject: [ConfigService],
        }),
        UserModule,
    ],
    providers: [
        // AuthService
        // LocalStrategy, // simply by importing them will register them to passport (under the hood it calls `passport.use(...)`)
        // LocalSerializer,
        // LocalAuthGuard,
    ]
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        consumer.apply(contextMiddleware).forRoutes('*');
    }
}
