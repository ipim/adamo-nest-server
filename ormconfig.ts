/* tslint:disable:quotemark object-literal-sort-keys */
import { SnakeNamingStrategy } from './src/snake-naming.strategy';
import * as dotenv from 'dotenv';

if (!(<any>module).hot /* for webpack HMR */) {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
}

dotenv.config({
    path: `.${process.env.NODE_ENV}.env`,
});

// Replace \\n with \n to support multiline strings in AWS
for (const envName of Object.keys(process.env)) {
    process.env[envName] = process.env[envName].replace(/\\n/g, '\n');
}

module.exports = {
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    username: process.env.POSTGRES_USERNAME,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE,
    namingStrategy: new SnakeNamingStrategy(),
    synchronise:true,
    migrationsRun: true,
    entities: [
        'src/modules/**/*.entity{.ts}',
        // 'dist/modules/**/*.entity{.js}',
    ],
    migrations: [
        'src/migrations/*{.ts}',
    ],
    // seeds: ['src/database/seeders/**/*.seeder.ts'],
    // factories: ['/src/database/factories/**/*.factory.ts'],
    cli: {
        // "entitiesDir": "src/entity",
        migrationsDir: 'src/migrations/*{.ts}',
        // "subscribersDir": "src/subscriber"
      }
};

import './src/boilerplate.polyfill';

import * as dotenv from 'dotenv';

import { SnakeNamingStrategy } from './src/snake-naming.strategy';

if (!(<any>module).hot /* for webpack HMR */) {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
}

dotenv.config({
    path: `.${process.env.NODE_ENV}.env`,
});

// Replace \\n with \n to support multiline strings in AWS
for (const envName of Object.keys(process.env)) {
    process.env[envName] = process.env[envName].replace(/\\n/g, '\n');
}

module.exports = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    namingStrategy: new SnakeNamingStrategy(),
    entities: ['src/modules/**/*.entity{.ts,.js}'],
    migrations: ['src/migrations/*{.ts,.js}'],
};

/* tslint:disable:quotemark object-literal-sort-keys */
import { SnakeNamingStrategy } from './src/snake-naming.strategy';
import * as dotenv from 'dotenv';

if (!(<any>module).hot /* for webpack HMR */) {
    process.env.NODE_ENV = process.env.NODE_ENV || 'development';
}

dotenv.config({
    path: `.${process.env.NODE_ENV}.env`,
});

// Replace \\n with \n to support multiline strings in AWS
for (const envName of Object.keys(process.env)) {
    process.env[envName] = process.env[envName].replace(/\\n/g, '\n');
}

module.exports = {
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: +process.env.POSTGRES_PORT,
    username: process.env.POSTGRES_USERNAME,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE,
    namingStrategy: new SnakeNamingStrategy(),
    synchronise:true,
    migrationsRun: true,
    entities: [
        'src/modules/**/*.entity{.ts}',
        // 'dist/modules/**/*.entity{.js}',
    ],
    migrations: [
        'src/migrations/*{.ts}',
    ],
    // seeds: ['src/database/seeders/**/*.seeder.ts'],
    // factories: ['/src/database/factories/**/*.factory.ts'],
    cli: {
        // "entitiesDir": "src/entity",
        migrationsDir: 'src/migrations/*{.ts}',
        // "subscribersDir": "src/subscriber"
      }
};
