FROM node:12.13 as base

#RUN apk --no-cache add --virtual native-deps \
#  g++ gcc libgcc libstdc++ linux-headers autoconf automake make nasm python git && \
#  npm install --quiet node-gyp -g


FROM base as builder

#ARG NODE_ENV=docker
#ENV NODE_ENV=${NODE_ENV}
COPY package.json ./
#RUN npm set progress=false && npm config set depth 0
RUN npm install
COPY . .
RUN npm run build

FROM base as dependencies
COPY package.json ./
RUN npm set progress=false && npm config set depth 0
# RUN npm install --only=production
RUN npm install

#RUN apk del .gyp

FROM node:alpine as release
COPY package.json ./
COPY nodemon.json ./

COPY --from=dependencies  /node_modules ./node_modules
COPY --from=builder /dist ./dist

CMD [ "npm","run", "start:hmr" ]
