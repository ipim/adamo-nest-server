# FROM node:10 AS dist
# # COPY package.json yarn.lock ./
# ARG NODE_ENV=production
# ENV NODE_ENV $NODE_ENV

# COPY package.json package-lock.json ./
# RUN npm ci 
# #--only=production
# COPY . .
# RUN npm run build

# # FROM node:alpine
# ARG PORT=3330
# # RUN mkdir -p /usr/src/app
# # WORKDIR /usr/src/app
# # COPY --from=dist dist /usr/src/app/dist
# # COPY --from=dist package.json /usr/src/app/
# # COPY --from=node_modules node_modules /usr/src/app/node_modules
# # COPY . /usr/src/app

# EXPOSE $PORT
# # CMD [ "npm", "run", "start:prod" ]
# # CMD [ "npm", "run", "seed"]


# FROM node:12.13 As development

# WORKDIR /usr/src/app

# COPY package*.json ./

# RUN npm install 
# #--only=development

# COPY . .

# RUN npm run build
# # RUN npm run migration:run

FROM node:12.13 as production

ARG NODE_ENV=docker
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
# --only=production
# RUN npm install -g ts-node
# RUN npm install -g pm2
COPY . .

# COPY --from=development /usr/src/app/dist ./dist

# CMD ["node", "dist/main"]
# CMD ["npx",  "pm2", "start", "pm2.ecosystem.config.js --no-daemon"]
# RUN npm run start:dev
